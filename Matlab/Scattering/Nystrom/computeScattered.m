function sm=computeScattered(k,m,vec,X,v,Y,phi)
% given mesh point vec
% k wave number 
% m half of the quadrature points
% v normal unit vector
% X,Y parametrizations 
% the density phi, solution of the integral equation 

% output: scattered wave computed on the mesh given by an integral, see Colton-Kress

dist=zeros(2*m,1);
for i=1:2*m
    dist(i)=sqrt((vec(1)-X(i,1))^2+(vec(2)-X(i,2))^2);
end
X1=zeros(2*m,1);
for j=1:2*m
    X1(j)=dot(vec-X(j,:),v(j,:))/dist(j);
end

% quadrature
sm=0;
for j=1:2*m
    sm=sm+pi/m*(1i*k/4*besselh(1,k*dist(j))*X1(j)+k/4*besselh(0,k*dist(j)))*phi(j)*sqrt(Y(j));
end


end