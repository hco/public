function ret=if_boundary(m,vec,X)

for i=1:2*m
    if(abs(vec(1)-X(i,1))<1e-10 && abs(vec(2)-X(i,2))<1e-10)
        ret=1;
        return
    else
        ret=0;
    end
end