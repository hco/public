function sm=computeScatUnitCircle(k,m,vec,X,Y,phi_exact)
% vec given mesh point 
% k wave number 
% m half of the quadrature points
% X,Y parametrizations 
% the density phi_exact

% output: scattered wave computed on the mesh

dist=zeros(2*m,1);
for i=1:2*m
    dist(i)=sqrt((vec(1)-X(i,1))^2+(vec(2)-X(i,2))^2);
end

% 
sm=0;
for j=1:2*m
    sm=sm-pi/m*(1i/4*besselh(0,k*dist(j))*phi_exact(j)*sqrt(Y(j)));
end




end
