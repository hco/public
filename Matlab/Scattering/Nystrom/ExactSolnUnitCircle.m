
% Exact solution for direct acoustic obstacle scattering in \R^2
% where the obstacle is the unit cirle
% H. Cagan Ozen Sep 2011
clear

m=64;  % half of the quadrature points
k=10;  % wave number
eta=k; % combined integral equation parameter

%set of knots t_i=pi*i/m j=0,..,2m-1
t = pi*(0:(2*m-1))/m;
d=[1 0]; %direction vector

%% parametrizatin of the obstacle
% unit circle parametrization
x1= cos(t);
x2= sin(t);
% %derivatives
dx1= -sin(t);
dx2= cos(t);
% %second derivatives
d2x1= -cos(t);
d2x2= -sin(t);
x1=x1';x2=x2';dx1=dx1';dx2=dx2';d2x1=d2x1';d2x2=d2x2';

X= [x1 x2];       % X(t)=(x1(t),x2(t))
dX= [dx1 dx2];    % dX(t)=(dx1(t),dx2(t))
d2X= [d2x1 d2x2]; % d2X(t)=(d2x1(t),d2x2(t))

%% Compute exact density by a formula
% formula is valid only for the unit circle

Y= (dx1.^2+dx2.^2);
v1= dx2./(Y.^(1/2));   
v2= -dx1./(Y.^(1/2)); 

%vector form of unit normal: v(t)=(v1(t),v2(t))
v=[v1 v2];
%not unit normal
nv=[dx2 -dx1];


N=600;
mvec=-N:N;
Ebessel=besselh(mvec,k);

exactDensity=zeros(2*m,1);
for j=1:(2*m)
    exactDensity(j)=2*1i/pi*sum(exp(1i*mvec*(t(j)+pi/2))./Ebessel);
end
exactDensity=-exactDensity;


[XX,YY]=meshgrid(-3:.05:3,-3:.05:3);

uinc=exp(1i*k*(XX));

sz=size(XX,1);
msh=cell(sz,sz); %meshgrid
for i=1:sz
    for j=1:sz
        msh{i,j}=[XX(i,j) YY(i,j)];
    end
end

scat=zeros(sz,sz); %scattered field
sameIndex=[]; % indices where mesh and boundary coincide!!

for i=1:sz
    for j=1:sz
        scat(i,j)=computeScatUnitCircle(k,m,msh{i,j},X,Y,exactDensity);
        if(if_boundary(m,msh{i,j},X))
            sameIndex=[sameIndex; [i j]];
        end
    end
end

% boundary values correction
for i=1:size(sameIndex,1)
    scat(sameIndex(i,1),sameIndex(i,2))= -uinc(sameIndex(i,1),sameIndex(i,2)); % set -uinc
end

% figure
colormap(copper)
imagesc([-3 3],[-3 3],real(scat))
hold on
fill(x1,x2,'k')
axis off







