
% Nystrom Method for direct acoustic obstacle scattering in \R^2
% Obstacles: kite-shaped or unit cirle
% H. Cagan Ozen Sep 2011
% Uses the density in the Colton-Kress

clear
format long
tic

m=64; % half of the quadrature points
k=10; % wave number, high frequency requires larger m
eta=k; % combined integral equation parameter

%set of knots t_i=pi*i/m j=0,..,2m-1
t = pi*(0:(2*m-1))/m;
d=[1 0]; %direction vector

shape = 'kite'; % or 'circle'

%%  parametrizatin of the obstacle

switch shape
    case 'kite' 
        x1=cos(t)+0.65*cos(2*t)-0.65;
        x2= 1.5*sin(t);
        %derivatives
        dx1= -sin(t)-2*0.65*sin(2*t);
        dx2= 1.5*cos(t);
        %second derivatives
        d2x1= -cos(t)-2*2*0.65*cos(2*t);
        d2x2= -1.5*sin(t);
    case 'circle'
        x1= cos(t);
        x2= sin(t);
        % derivatives
        dx1= -sin(t);
        dx2= cos(t);
        % second derivatives
        d2x1= -cos(t);
        d2x2= -sin(t);    
    otherwise
        error('Shape not implemented');
end
x1=x1';x2=x2';dx1=dx1';dx2=dx2';d2x1=d2x1';d2x2=d2x2';

X= [x1 x2];       % X(t)=(x1(t),x2(t))
dX= [dx1 dx2];    % dX(t)=(dx1(t),dx2(t))
d2X= [d2x1 d2x2]; % d2X(t)=(d2x1(t),d2x2(t))

%% vectorized initialization 

Y= (dx1.^2+dx2.^2);
v1= dx2./(Y.^(1/2));   
v2= -dx1./(Y.^(1/2)); 

%vector form of unit normal: v(t)=(v1(t),v2(t))
v=[v1 v2];
%not unit normal
nv=[dx2 -dx1];

dvec=repmat(d,2*m,1); % replicate direction vector

% incoming wave
Uinc= exp(1i*k*dot(X,dvec,2));
% right hand side of the system
g= -2 * Uinc;

%Vectorized Euclidean distance
R = sqrt(sum(abs( repmat(permute(X, [1 3 2]), [1 2*m 1]) ...
                - repmat(permute(X, [3 1 2]), [2*m 1 1]) ).^2, 3));

%X2= (nv1(s),nv2(s)) \cdot (x(t)-x(s))            
X2= dot( repmat(permute(X, [1 3 2]), [1 2*m 1]) ...
    - repmat(permute(X, [3 1 2]), [2*m 1 1]), repmat(permute(nv, [3 1 2]), [2*m 1 1]),3);            
X1= -X2;

T= (repmat(t,[2*m 1])' - repmat(t,[2*m 1])); % ti -tj matrix
Lg= log(4* sin( T./2).^2);

% quadrature weights matrix, see Colton-Kress or Kress Integral Equations
Rm= (-2*pi/m) * (sum(repmat(reshape(1./(1:(m-1)),[1 1 m-1]),[2*m 2*m]).*(cos(reshape(T(:)*(1:(m-1)),[2*m 2*m m-1]))),3) + 1/(2*m)*cos(m*T) );


%% Computation of kernels of the integral equation
% L=zeros(2*m,2*m);  % 1i*(k/2)*X1(ti,tj)*(besselj(1,k*R(ti,tj))+1i*bessely(1,k*R(ti,tj)))*(R(ti,tj)^(-1))
% L1=zeros(2*m,2*m); % k/(2*pi)*X2(ti,tj)*besselj(1,k*R(ti,tj))*(R(ti,tj)^(-1))                  
% L2=zeros(2*m,2*m); % L(ti,tj)-L1(ti,tj)*log(4*sin((ti-tj)/2)^2)
% M=zeros(2*m,2*m);  % (1i/2)*(besselj(0,k*R(ti,tj))+1i*bessely(0,k*R(ti,tj)))*(Y(tj)^(1/2))
% M1=zeros(2*m,2*m); % -1/(2*pi)*besselj(0,k*R(ti,tj))*(Y(tj)^(1/2))
% M2=zeros(2*m,2*m); % M(ti,tj)-M1(ti,tj)*log(4*sin((ti-tj)/2)^2)
C=0.577215664901532860; % Euler's constant

M= (1i/2)*besselh(0,k*R).*repmat((Y.^(1/2))',[2*m 1]);
M1= -1/(2*pi)*besselj(0,k*R).*repmat((Y.^(1/2))',[2*m 1]);

M2= M-M1.*Lg;
L= 1i*(k/2)*X1.*(besselh(1,k*R)./R);
L1= k/(2*pi)*X2.*(besselj(1,k*R)./R);
L2= L-L1.*Lg;

diagindex=1:2*m+1:(2*m)*(2*m);
M2(diagindex)=((1i/2)-(C/pi)-1/(pi)*log(k/2*sqrt(Y))).*(Y.^(1/2));
L1(diagindex)=0;
L2(diagindex)=-1/(2*pi)*diag(dot(repmat(permute(d2X, [1 3 2]), [1 2*m 1]), ...
                              repmat(permute(nv, [1 3 2]), [1 2*m 1]),3))./Y;
                           
K1= L1 + 1i * eta * M1;
K2= L2 + 1i * eta * M2;
K= Rm.*K1+(pi/m)*K2;
I=eye(2*m,2*m);
A=I-K; % linear system matrix

%% Solving for the density phi & computation of far field pattern

%solve A*phi=g for phi
phi=A\g;
c=(pi/m)*(exp(-(1i)*pi/4)/(sqrt(8*pi*k)));
xbar=d;
far_field=c * sum((k*dot(v,repmat(xbar,2*m,1),2)+eta).*exp(-1i*k*(dot(X,repmat(xbar,2*m,1),2))).*phi.*(Y.^(1/2)));


%% plot the scattering wave

[XX,YY]=meshgrid(-3:.05:3,-3:.05:3);

uinc=exp(1i*k*(XX));

sz=size(XX,1);
msh=cell(sz,sz); % meshgrid
for i=1:sz
    for j=1:sz
        msh{i,j}=[XX(i,j) YY(i,j)];
    end
end

scat=zeros(sz,sz); %scattered field
sameIndex=[]; % indices where mesh and boundary coincide
for i=1:sz
    for j=1:sz
        scat(i,j)=computeScattered(k,m,msh{i,j},X,v,Y,phi);
        if(if_boundary(m,msh{i,j},X))
            sameIndex=[sameIndex; [i j]];
        end
    end
end
% boundary values correction
for i=1:size(sameIndex,1)
    scat(sameIndex(i,1),sameIndex(i,2))= -uinc(sameIndex(i,1),sameIndex(i,2)); % set -uinc
end

figure
colormap(copper)
imagesc([-3 3],[-3 3],real(scat))
hold on
fill(x1,x2,'k')
axis off

figure
colormap(copper)
imagesc([-3 3],[-3 3],real(uinc)+real(scat))
hold on
fill(x1,x2,'k')


toc




