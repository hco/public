function A=AssembleGalerkinMatrix(RnK,WK,Basis,cBasis,w,d1,d2,d3,d4,quadCell)
% FAST Serial Galerkin matrix assembly using quadrature
% uses properties of partition of unity to reduce the number of quadrature points
% exclude regions from the integration by checking the cut-off functions
% H. Cagan Ozen June 12


% Inputs
% RnK   : Rn.*K1
% WK    : W.* K2
% Basis : polynomial basis matrix
% cBasis: conjuagate of basis
% w     : quad weights
% d1,d2,d3,d4 : used degrees of freedom (4 regions)

% quadCell  : a cell that contains quadrature points for double integrals coming
% from different regions. E.g. if cut1+cut4 involves in the integral then
% quadCell{N,M}= t14;

d=uint16(d1+d2+d3+d4+4);
A=zeros(d,d); % Galerkin Matrix
fprintf('\nAssembling galerkin matrix...\n');
u1=uint8(1);
u2=uint16(1);
for N=u1:d
    for M=u1:d
%       %integrand Basis(:,M)*cBasis(:,N) %if(check_cuts(N,M,d1))
        %single integral
        sum1=sum(w.*Basis(:,M).*cBasis(:,N));
        
        vec=quadCell{N,M};
        sz=uint16(size(vec,2));
        sum2=0;
        for i=u2:sz
            sum2=sum2+w(vec(i))*cBasis(vec(i),N)*sum(RnK(vec(i),:).*(Basis(:,M).')); 
        end 
        
        %vectorized double integration
        sum3=0;
        for i=u2:sz
            sum3=sum3+cBasis(vec(i),N)*sum(WK(vec(i),:).*(Basis(:,M).')); 
        end 

        A(N,M)=sum1-(sum2+sum3);
    end
end


end