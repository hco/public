% Initialize 4 regions with cut_offs
% corresponds to m=1 in the algorithm
function regions= InitRegions(nReg, k, t1,t2,eps,t)

alpha1=7*pi/15;
alpha2=alpha1;

beta1=pi;
beta2=beta1;

alpha3=pi/5;
alpha4=pi/5;

c1=2*pi/3;
c2=2*pi/3;

regions = cell(nReg,1);

%Intervals 
regions{1}.int=[t1-beta1*k^(-1/3+eps), t1+alpha1*k^(-1/3+eps)];
regions{2}.int=[t2-alpha2*k^(-1/3+eps), t2+beta2*k^(-1/3+eps)];
regions{3}.int=[t1+alpha3*k^(-1/3+eps), t2-alpha4*k^(-1/3+eps)];
regions{4}.int1=[0,t1-c1*k^(-1/3+eps)];
regions{4}.int2=[t2+c2*k^(-1/3+eps), 2*pi];

% Characteristic Functions
regions{1}.char=CharFunc(regions{1}.int(1),regions{1}.int(2),t);
regions{2}.char=CharFunc(regions{2}.int(1),regions{2}.int(2),t);
regions{3}.char=CharFunc(regions{3}.int(1),regions{3}.int(2),t);
regions{4}.char1=CharFunc(regions{4}.int1(1),regions{4}.int1(2),t);
regions{4}.char2=CharFunc(regions{4}.int2(1),regions{4}.int2(2),t);
regions{4}.char=regions{4}.char1+regions{4}.char2;

% Cut-off functions
regions{3}.cut=cutOffBoth(regions{3}.int(1),regions{1}.int(2),regions{2}.int(1),regions{3}.int(2),t);
regions{1}.cut=cutOffBoth(regions{1}.int(1),regions{4}.int1(2),regions{3}.int(1),regions{1}.int(2),t);
regions{2}.cut=cutOffBoth(regions{2}.int(1),regions{3}.int(2),regions{4}.int2(1),regions{2}.int(2),t);

regions{4}.cut1=cutOffRight(regions{1}.int(1),regions{4}.int1(2),t,regions{4}.char1);
regions{4}.cut2=cutOffLeft(regions{4}.int2(1),regions{2}.int(2),t,regions{4}.char2);
regions{4}.cut=regions{4}.cut1+regions{4}.cut2;

% middle points
regions{1}.mid=(regions{1}.int(1)+regions{1}.int(2))/2;
regions{2}.mid=(regions{2}.int(1)+regions{2}.int(2))/2;
regions{3}.mid=(regions{3}.int(1)+regions{3}.int(2))/2;
regions{4}.mid1=(regions{4}.int1(1)+regions{4}.int1(2))/2;
regions{4}.mid2=(regions{4}.int2(1)+regions{4}.int2(2))/2;

end

