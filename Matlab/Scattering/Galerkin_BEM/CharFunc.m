% Characteristics funtions for regions
function Return=CharFunc(a,b,t)
%n:half of quad points

%using function handle
S=@(x) (x>=a & x<=b)*1+(x<a)*0+(x>b)*0 ;

Return=S(t).';

end