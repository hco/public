
function Rn=quadLogTerm(t,n)
% Quadrature weights matrix
% for the logarithmic lingularity
% See Kress Linear Integral equations
m=1:(n-1);
Rn2=zeros(2*n,1);

for i=1:(2*n)
    Rn2(i)=(-2*pi/n)*sum((1./m).*cos(m*(t(i))))-(-1)^(i-1)*pi/(n^2);
end
% Create the circulant matrix Rn
Rn=gallery('circul',Rn2);


end
