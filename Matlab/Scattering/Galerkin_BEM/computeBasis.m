function [Basis, cBasis] = computeBasis(l, d, d1,d2,d3, t, regions)

%Basis(:,N)=basisfuncN & Basis(i,N)=basisfuncN(ti)
Basis=zeros(l,d);
n=l/2;

t=t.'; 
for N=1:d
        if ( N<=(d1+1) ) 
            if(N==1)
                Basis(:,N)=regions{1}.cut;
            else
                Basis(:,N)= regions{1}.cut.*((t-regions{1}.mid)).^(N-1); 
            end
        elseif ( N>(d1+1) && N<=(d1+1+d2+1) )
            if(N==d1+2)
                Basis(:,N)=regions{3}.cut;
            else
                Basis(:,N)= regions{3}.cut.*((t-regions{3}.mid)).^(N-1-d1-1);  
            end
        elseif( N>(d1+1+d2+1) && N<=(d1+1+d2+1+d3+1) )
            if(N==d1+1+d2+1+1)
                Basis(:,N)=regions{2}.cut;
            else
                Basis(:,N)= regions{2}.cut.*((t-regions{2}.cut)).^(N-1-d1-1-d2-1);   
            end
        end
end
    
for i=1:l
    for N=(d1+1+d2+1+d3+1+1):d
        if (t(i)>=0 && t(i) <pi/2)
            Basis(i,N) = regions{4}.cut1(i)*(t(i)-regions{4}.mid1)^(N-1-(d1+1+d2+1+d3+1));
        else
           Basis(i,N) = regions{4}.cut2(i)*(t(i)-regions{4}.mid2)^(N-1-(d1+1+d2+1+d3+1)); 
        end
    end
end
    
   

w= pi/n*ones(2*n,1);

for N=1:d
    Basis(:,N)=sqrt(sum(w.*abs(Basis(:,N)).^2))^(-1)*Basis(:,N);
end  

%cBasis=conjugate(Basis)
cBasis=conj(Basis);

end