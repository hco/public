% initialize parametrization of the obstacle
% requires symbolic math toolbox

function [x1,x2,dx1,dx2,d2x1,d2x2]=initialize(t)

  syms x;
  x11=cos(x); x22=sin(x);
  dx11=diff(x11); dx22=diff(x22);
  d2x11=diff(dx11); d2x22=diff(dx22);
  x1= subs(x11,t)';
  x2= subs(x22,t)';
  dx1= subs(dx11,t)';
  dx2= subs(dx22,t)';
  d2x1= subs(d2x11,t)';
  d2x2= subs(d2x22,t)';
    

end