%% cut-off  left-sided bump function
%
% [a---c------d]
% a < c < d
% smooth between a&c

function ss=cutOffLeft(a,c,t,char)
%n:half of quad points

myc=2;

%vectorized
S=@(x) (1-((x<=a)*1+(x<c & x>a).*exp(myc*(c-a)./(x-c).*exp((a-c)./(x-a)))));
ss=1/2*(S(t)+1-S(c-(t-a))).';

x=isnan(ss);
if(~isequal(x,zeros(size(t))'))
xx=find(x==1);
for i=1:size(xx,1)
    if(abs(ss(xx(i)-1)-ss(xx(i)+1))<1e-5)
        ss(xx(i))=round(ss(xx(i)+1));   
    else
        disp('\n ERROR NAN IN CUT-OFF\n');
    end
end
    
end


end




