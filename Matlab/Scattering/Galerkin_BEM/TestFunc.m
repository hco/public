function TestFunc(k,n,d1,d2,d3,d4)
% Scattering from the unit circle 
% using partition of unity consisting of 4 regions
% Approximation spaces: Normalized polynomials
% H. Cagan Ozen June 12



% Inputs
% k = Wave number
% n = half of the quadrature points
% d1= degree of freedom used in Shadow boundary 1
% d2= degree of freedom used in illuminated
% d3= degree of freedom used in Shadow boundary 2 
% d4= degree of freedom used in deep shadow  (2di+1 used in implementation)

% Outputs
% Relative errors and condition number of the linear system

format long

fprintf('Wave number is k=%d\n',k);
a=[1 0]'; %direction vector
coup=k;  % coupling parameter
l=2*n;
nReg=4;  % number of regions

%% Parametrization of the obstacle

%set of knots t_i=pi*(i-1)/n j=1,..,2n
t = pi*(0:(2*n-1))/n;

[x1,x2,dx1,dx2,d2x1,d2x2]=initialize(t); 
%vector forms of parametrizations
X= [x1 x2];       % X(t)=(x1(t),x2(t))
dX= [dx1 dx2];    % dX(t)=(dx1(t),dx2(t))
d2X= [d2x1 d2x2]; % d2X(t)=(d2x1(t),d2x2(t))

% Y    % dx1(t)^2+dx2(t)^2
% v1   % unit normal(outward) first component 
% v2   % unit normal second component
Y= (dx1.^2+dx2.^2).^(1/2);
YY=repmat((Y)',[2*n 1]); % repmat |x'(s)|
v1= dx2./Y;   
v2= -dx1./Y; 
%vector form of unit normal: v(t)=(v1(t),v2(t))
v=[v1 v2];

avec=repmat(a',2*n,1); % replicate direction vector

%% Vectorized input functions
%Uinc  % Uinc(x)= exp(ikd.x) % incoming wave
%g     % RHS, f(x)=2*dUinc(x)/dv(x) & g(t)=f(x(t)) & g(i)=g(ti)
%R=zeros(2*n+1,2*n+1);  % {[x1(ti)-x1(tj)]^2+[x2(ti)-x2(tj)]^2}^(1/2), Euclidean distance
%X1=zeros(2*n+1,2*n+1); % (v1(t),v2(t)) \cdot (X(t)-X(s))

Uinc= exp(1i*k*dot(X,avec,2));
g=2*1i*(dot(v,avec,2))-2*1i*coup/k;
%Vectorized Euclidean distance
XX=repmat(permute(X, [1 3 2]), [1 2*n 1])- repmat(permute(X, [3 1 2]), [2*n 1 1]); 
R = sqrt(sum(abs( XX ).^2, 3)); 

vv=repmat(permute(v, [1 3 2]), [1 2*n 1]);
X1= dot(XX, vv ,3); % WITH UNIT NORMAL

T= (repmat(t,[2*n 1])' - repmat(t,[2*n 1])); % ti -tj matrix
Lg= log(4* sin( T./2).^2); % Logarithmic singularity term
% quadrature weights matrix
Rn= quadLogTerm(t,n); % more efficient

%Ex(i,j)= exp(1i*k*(a(1)*(x1(j)-x1(i))+a(2)*(x2(j)-x1(i))));
Ex= exp(1i*k*dot(-XX,(repmat(permute(avec, [1 3 2]), [1 2*n 1])),3));

%%  Vectorized KERNELS L & M of the integral equation

C=0.577215664901532860; % Euler's constant
L= 1i*k/2*(X1./R).*(besselh(1,k*R)).*YY; %X1(t,s)*Y
L1= -k/(2*pi)*(X1./R).*(besselj(1,k*R)).*YY;
L2= L-L1.*Lg;

M= (1i/2)*besselh(0,k*R).*YY;
M1= -1/(2*pi)*besselj(0,k*R).*YY;
M2= M-M1.*Lg;

%diagonals
diagindex=1:2*n+1:(2*n)*(2*n);
M2(diagindex)= ((1i/2)-(C/pi)-1/(pi)*log(k/2*Y)).*Y;
L1(diagindex)=0;
L2(diagindex)=-1/(2*pi)*diag(dot(repmat(permute(d2X, [1 3 2]), [1 2*n 1]), vv ,3))./Y;
                          
K1= L1 + 1i * coup * M1;
K2= L2 + 1i * coup * M2;

K1=K1.*Ex;
K2=K2.*Ex;

%% Regions and Basis functions
% total degree of polynomials
d=d1+d2+d3+d4+4;
% initialize region parameters 
% cut-off functions
eps=1/9;
t1=pi/2;
t2=3*pi/2;
regions=InitRegions(nReg, k, t1,t2,eps,t);

% Compute corresponding basis functions using cutoff functions
[Basis, cBasis] = computeBasis(l, d, d1,d2,d3, t, regions);

%% Compute Right side
b=zeros(d,1); % RHS in Galerkin formulation
%weights for single integrals, TRAp for trig
w= pi/n*ones(1,2*n);
%Weight matrix for double integrals
W=w'*w; 
w=w';
% Assembling RHS
for N=1:d
   b(N)=sum(w.*g.*cBasis(:,N)); 
end

%% SERIAL assembly of the galerkin matrix

WK=W.*K2; %W(i,j)*K2(i,j) nonsingular
RnK=Rn.*K1; %Rn(i,j)*K1(i,j) singular

quadCell=quadPtsRegions(regions{1}.int,regions{2}.int,regions{3}.int,regions{4}.int1,regions{4}.int2,t,d1,d2,d3,d4);
A=AssembleGalerkinMatrix(RnK,WK,Basis,cBasis,w,d1,d2,d3,d4,quadCell);

fprintf('\nCondition number for Galerkin matrix:');
condA=cond(A)
% Solve the system
u=A\b;

%% compute density eta of the integral equation
% slow density
etaslow=zeros(l,1);
for i=1:(l)
   etaslow(i)=etaslow(i)+sum(u.*(Basis(i,:).'));
end
% oscillatory eta by the Galerkin method
eta=k*etaslow.*Uinc;


%%  Exact density solution computation
t=t-pi/2;
N=2000;
mvec=-N:N;
Ebessel=besselh(mvec,k);

exact=zeros(2*n,1);

for j=1:(2*n)
    exact(j)=2*1i/pi*sum(exp(-1i*mvec*t(j))./Ebessel);
end
exact=-exact;

% Relative norm errors before preconditioning
[Nr,Nk]=computeErrors(eta,exact,regions{1}.char,regions{2}.char,regions{3}.char,regions{4}.char,w,k);
Norm_rel=Nr(1)
Normk=Nk(1)


%% Simple preconditioning

% Preconditioning: A --> A*B
B=zeros(d,d);
for i=1:d
    B(i,i)=norm(A(:,i),inf)^(-1);
end
fprintf('\nCondition number for Preconditioned Galerkin matrix:');
condAB=cond(A*B)

x=(A*B)\b;
u=B*x;   %Solution of the Preconditioned Linear system

etaslow=zeros(l,1);
for i=1:(l)
        etaslow(i)=etaslow(i)+sum(u.*(Basis(i,:).'));
end
eta=k*etaslow.*Uinc;


% errors after preconditioning
[MP_Nr,MP_Nk]=computeErrors(eta,exact,regions{1}.char,regions{2}.char,regions{3}.char,regions{4}.char,w,k);
Norm_rel=MP_Nr(1)
Normk=MP_Nk(1)

% save all norms and condition numbers
savefile = strcat('poly_4_k',num2str(k),'n',num2str(n),'d',num2str(d));
save(savefile, 'Nr', 'Nk','MP_Nr','MP_Nk','condA','condAB');


fprintf('\n-------------\n')


end
