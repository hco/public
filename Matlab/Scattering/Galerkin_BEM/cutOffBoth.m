%% cutoff function [a---c----d----b]
% illuminated region 

function ss=cutOffBoth(a,c,d,b,t)
%n:half of quad points
%a: left end of the interval
%b: rightend of the interval

myc=2;
% 

%vectorized
Sr=@(x) ((x<=d)*1+(x<b & x>d).*exp(myc*(b-d)./(x-b).*exp((d-b)./(x-d)))+(x>=b)*0);
Sl=@(x) (1-((x<=a)*1+(x<c & x>a).*exp(myc*(c-a)./(x-c).*exp((a-c)./(x-a)))+(x>=c)*0));
sl=1/2*(Sl(t)+1-Sl(c-(t-a)));
sr=1/2*(Sr(t)+1-Sr(b-(t-d)));

ss=(sr.*sl).';

x=isnan(ss);
if(~isequal(x,zeros(size(t))'))
xx=find(x==1);
for i=1:size(xx,1)
    if(isnan(ss(1)))
        ss(1)=ss(2);
        disp('\n ERROR NAN IN zeroth place\n');
    elseif(abs(ss(xx(i)-1)-ss(xx(i)+1))<1e-4)
        ss(xx(i))=round(ss(xx(i)+1));
    else
        disp('\n ERROR NAN IN CUT\n');
    end
end
    
end

end



