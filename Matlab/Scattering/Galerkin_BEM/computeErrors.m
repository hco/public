function [Norm_r_vec,Norm_k_vec]=computeErrors(eta,exact,char1,char2,char3,char4,w,k) 
% Calculate relative errors for total 4 regions

Norm_eta_exact=sqrt(sum(w.*(abs((eta-exact)).^2)));
Norm_exact=sqrt(sum(w.*(abs(exact).^2)));
Norm_relative= Norm_eta_exact/Norm_exact;
Norm_k= Norm_eta_exact/k;

% Region 1
Norm1=sqrt(sum(w.*(abs(char1.*(eta-exact)).^2)));
Norm_e1=sqrt(sum(char1.*w.*(abs(exact).^2)));
Norm_r1=Norm1/Norm_e1;
Norm_k1=Norm1/k;

% Region 2
Norm2=sqrt(sum(char2.*w.*(abs((eta-exact)).^2)));
Norm_e2=sqrt(sum(char2.*w.*(abs(exact).^2)));
Norm_r2=Norm2/Norm_e2;
Norm_k2=Norm2/k;

% Region 3
Norm3=sqrt(sum(char3.*w.*(abs((eta-exact)).^2)));
Norm_e3=sqrt(sum(char3.*w.*(abs(exact).^2)));
Norm_r3=Norm3/Norm_e3;
Norm_k3=Norm3/k;

% Region 4
Norm4=sqrt(sum(char4.*w.*(abs((eta-exact)).^2)));
Norm_e4=sqrt(sum(char4.*w.*(abs(exact).^2)));
Norm_r4=Norm4/Norm_e4;
Norm_k4=Norm4/k;


% [Total 1 2 3 4]
Norm_r_vec=[Norm_relative Norm_r1 Norm_r2 Norm_r3 Norm_r4]; 
Norm_k_vec=[Norm_k Norm_k1 Norm_k2 Norm_k3 Norm_k4];
end

