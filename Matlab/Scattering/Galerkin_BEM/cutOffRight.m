%%  right-sided bump function on
% [a-------c---d]
% a < c < d
% smooth betwenn c&d

function ss=cutOffRight(c,d,t,char)
%n:half of quad points

myc=2;
S=@(x) ((x<=c)*1+(x<d & x>c).*exp(myc*(d-c)./(x-d).*exp((c-d)./(x-c)))+(x>=d)*0);
ss=1/2*(S(t)+1-S(d-(t-c))).';

x=isnan(ss);
if(~isequal(x,zeros(size(t))'))
xx=find(x==1);
for i=1:size(xx,1)
    if(abs(ss(xx(i)-1)-ss(xx(i)+1))<1e-5)
        ss(xx(i))=round(ss(xx(i)+1));
    else
        disp('\n ERROR NAN IN CUT OFF\n');
    end
end
    
end

end



