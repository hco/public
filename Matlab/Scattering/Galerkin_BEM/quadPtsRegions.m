% find pts for corresponding regions in quadrature points t

% tij contains quadrature pts for only region i and j

% c is a cell that contains quadrature points for double integrals coming
% from different regions. E.g. if cut1+cut4 involves in the integral then
% c{N,M}=c{M,N} t14;

function c= quadPtsRegions(int1,int2,int3,int41,int42,t,d1,d2,d3,d4)


vec1= find(int1(1)<=t);
vec2= find(int1(2)>=t);

t11=vec1(1):vec2(end);

vec3=find(int3(2)>=t);
vec4=find(int3(1)<=t);

t33=vec4(1):vec3(end);

t13=vec1(1):vec3(end);

vec5=find(int2(1)<=t);
vec6=find(int2(2)>=t);

t22=vec5(1):vec6(end);

t23=vec4(1):vec6(end);

t12=[t11 t22];


vec411= find(int41(1)<=t);
vec412= find(int41(2)>=t);
t411=vec411(1):vec412(end);

vec412= find(int42(1)<=t);
vec422= find(int42(2)>=t);
t422=vec412(1):vec422(end);

t14=[vec411(1):vec2(end) t422];

t44=[t411 t422];

t24=[t411 vec5(1):vec422(end)];

t34=[t44 t33];


d=d1+d2+d3+d4+4;
%%
c=cell(d,d);
for N=1:d
    for M=1:d
        if(N<=d1+1)
            if(M<=d1+1)
                c{N,M}=t11;
            elseif(d1+1<M && M<=d1+1+d2+1)
                c{N,M}=t13;
            elseif(d1+1+d2+1<M && M<=d1+1+d2+1+d3+1)
                c{N,M}=t12;
            elseif(d1+1+d2+1+d3+1<M && M<=d1+1+d2+1+d3+1+d4+1)
                c{N,M}=t14;  
            end


        elseif(d1+1<N && N<=d1+1+d2+1)
             if(M<=d1+1)
                c{N,M}=t13;
            elseif(d1+1<M && M<=d1+1+d2+1)
                c{N,M}=t33;
            elseif(d1+1+d2+1<M && M<=d1+1+d2+1+d3+1)
                c{N,M}=t23;
            elseif(d1+1+d2+1+d3+1<M && M<=d1+1+d2+1+d3+1+d4+1)
                c{N,M}=t34;  
            end


        elseif(d1+1+d2+1<N && N<=d1+1+d2+1+d3+1)
            if(M<=d1+1)
                c{N,M}=t12;
            elseif(d1+1<M && M<=d1+1+d2+1)
                c{N,M}=t23;
            elseif(d1+1+d2+1<M && M<=d1+1+d2+1+d3+1)
                c{N,M}=t22;
            elseif(d1+1+d2+1+d3+1<M && M<=d1+1+d2+1+d3+1+d4+1)
                c{N,M}=t24;  
            end

        elseif(d1+1+d2+1+d3+1<N && N<=d1+1+d2+1+d3+1+d4+1)
            if(M<=d1+1)
                c{N,M}=t14;
            elseif(d1+1<M && M<=d1+1+d2+1)
                c{N,M}=t34;
            elseif(d1+1+d2+1<M && M<=d1+1+d2+1+d3+1)
                c{N,M}=t24;
            elseif(d1+1+d2+1+d3+1<M && M<=d1+1+d2+1+d3+1+d4+1)
                c{N,M}=t44;  
            end

        end

    end
    
end



end



