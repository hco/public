#include <iostream>
#include "snsvor_aux.h"
#include <boost/timer.hpp>



/*
 * Stochastic Navier Stokes (SNS) driven by Brownian motion in two dimensions
 * with periodic boundary conditions (vorticity formulation)
 * 
 * 
 * Propagation of stochasticity is carried out by Dynamical gPC (https://arxiv.org/abs/1605.04604)
 * 
 * H. Cagan Ozen, June 2015
 * 
 * Requirements: Boost, Eigen, UQTk 2.0 or 3.0, ARPACK, FFTW
 */


int main(int argc, char *argv[])
{  
/*
    K : dimension in xi (dW)
    N : degree of polynomial is (u,xi)
    D1 : number of modes retained in Karhunen-Loeve for viscosity
    D2 : number of modes retained in Karhunen-Loeve for temperature
    D : D1+D2 total number of modes
    M : spatial resolution 
    Ti, Tf : initial and final time
    nt : number of time steps
    Res_no : number of restarts
    Nsamp : number of samples for moments
    FreqCut: Fourier frequency cut-off paramter
*/

    int K, N, D1, D2, D, M, nt, Res_no, Res_cnt, Nsamp, FreqCut;
    double Ti, Tf;

    if(argc>1){ // if command line arguments
        int i=1;
        K=atoi(argv[i++]); 
        N=atoi(argv[i++]);
        D1 =atoi(argv[i++]); 
        D2 =atoi(argv[i++]); 
        M = atoi(argv[i++]);
        Res_no = atoi(argv[i++]);
        Ti = atof(argv[i++]);
        Tf = atof(argv[i++]);
        Nsamp= atoi(argv[i++]);
        nt= atoi(argv[i++]);
        FreqCut=atoi(argv[i++]);
        Nsamp*=1000;
    }else{ // default values
        K=4;
        N=2;
        D1=2;
        D2=2;
        M=64;
        Res_no=9;
        Ti=0.0;
        Tf=1.0;
        nt=100;
        Nsamp=200000;
        FreqCut=0;
    }
    cout<<"-------------------------------------------------------------------"<<endl;
    cout<< "K"<<K << " N"<< N <<" D1"<<D1<<" D2"<<D2<<" M"<<M<<" Ti"<<Ti<<" Tf"<<Tf<<" Res_no"<<Res_no
                <<" Nsamp"<<Nsamp<<" nt"<<nt<<endl;

    // variables for the first interval
    int K0 = K;
    int N0 = N;

    double nu; // viscosity
    nu=0.00055;

    D=D1+D2;
    Res_cnt=0;
        
    /*    
      dsigma1 :   d_y \sigma_1 forcing term
      dsigma2 :   d_x \sigma_w forcing term
      kx : real 1d FFT frequency vector 
      ky : kx 
      k2 :  ky^2 + kx^2  
      ki : i*k
      Fdsigma1 : FFT of dsigma1 
      Fdsigma2 : FFT of dsigma2 
      x  :  spatial mesh for 1d 
    */
    VectorXd  x(M),kx(M),ky(M);
    VectorXcd ki(M);
    MatrixXd  dsigma1(M,M),dsigma2(M,M), k2(M,M);
    MatrixXcd Fdsigma1(M,M),Fdsigma2(M,M);
   
    // get fft frequencies for 1d
    FFT1dFreqVector(M,kx);
    ky=kx;

    //spatial mesh for 1d, equidistant points
    for(int i=0; i<M;++i) x(i) = Ti+ i*1.0/(double)M;
    
    // k^2 and forcing terms dsigma_1 and dsigma_2
    for(int i=0; i<M; ++i){ // x variable
        for(int j=0; j<M; ++j){ // y variable
            k2(j,i) = pow(ky(j),2) + pow(kx(i),2);
            dsigma1(j,i) = 0.1*M_PI*cos(2*M_PI*x(i))*cos(2*M_PI*x(j));
            dsigma2(j,i) = 0.1*M_PI*cos(2*M_PI*x(i))*sin(2*M_PI*x(j));
        }
    }
    //for odd derivatives set k(M/2)=0; loss of symmetry
    kx(M/2) =0;
    ky(M/2) =0;

    // row_major format, data format for fft
    fftw_plan fft_fwd, fft_inv;
    fftw_complex *in, *out, *inv;
    in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * M * M);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * M * M);
    inv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * M * M);

    /// initialize fft forward and inverse maps
    fft_fwd= fftw_plan_dft_2d(M, M, in, out, FFTW_FORWARD, FFTW_PATIENT);
    fft_inv= fftw_plan_dft_2d(M, M, out, inv, FFTW_BACKWARD, FFTW_PATIENT);

    // FFT forcing terms, writes to last variable
    fft2_eigenxdc(dsigma1, M, fft_fwd,in,out, Fdsigma1);
    fft2_eigenxdc(dsigma2, M, fft_fwd,in,out, Fdsigma2);

    // initial condition for vorticity
    // see [Hou et. al. '06]
    MatrixXd VorticityInit(M,M);
    VorticityInit.setZero();
    double nrm=0, eps=0.3, delta=0.25; 
    for(int j=0; j<M; ++j){ /// variable y
        for(int i=0; i<M; ++i){ /// variable x
            VorticityInit(j,i) =  1/(2*delta) * exp(-((1+ eps*(cos(4*M_PI*x(i))-1))*pow(x(j)-0.5,2))/(2*pow(delta,2)));
            nrm+= VorticityInit(j,i)/(double)(pow(M,2));
        }
    }
    VorticityInit = nrm*MatrixXd::Ones(M,M)- VorticityInit;
    
    int KD=K+D; // total random variables
    // sparse index initializations
    MatrixXi rvecxi(N0+1,K0), rvecxieta(N+1,KD);
    rvecxi.setZero();
    rvecxieta.setZero();

    // initialize sparse multi-index
    SetSparseIndex(K, N, D, K0,N0, rvecxi, rvecxieta);
    
    // main loop for Dynamical generalized Polynomial Chaos expansions
    // non adaptive restart rule with deterministic viscosity
    MainLoopSNS_DetVis( K, N, K0, N0, D,  M, Res_no, Res_cnt,  Nsamp, nt, FreqCut,
                                        Ti, Tf, VorticityInit, nu, Fdsigma1, Fdsigma2,
                                        kx, ky, k2, rvecxi, rvecxieta, fft_fwd, fft_inv, in, out, inv);

    fftw_destroy_plan(fft_fwd);
    fftw_destroy_plan(fft_inv);
    fftw_free(out);
    fftw_free(in);
    fftw_free(inv);

    return 0;
}



