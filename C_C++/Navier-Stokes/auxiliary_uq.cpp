#include "auxiliary_uq.h"


// \todo bound checks

/// choose(n,k)
unsigned long nChoosek( unsigned n, unsigned k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    unsigned long result = n;
    for( unsigned int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

/// double factorial
unsigned long double_factorial(int n)
{
    if (n==-1 || n == 0 || n == 1)
    return 1;

    return double_factorial(n - 2) * n;
}


/// print 1d vec
void print_vec(Array1D<double> data){
	int nd=data.XSize();
	for(int i=0; i<nd; i++){
		cout<< data(i)<< ' ';
	}
	cout<<'\n';
}

/// print 2d vec
void print_vec2d(Array2D<double> data){
	int nd=data.XSize();
	int K= data.YSize();
	cout.precision(4);
	for(int i=0; i<nd; i++){
		for(int j=0; j<K;j++)
			cout<<  std::fixed<<std::setprecision(10)<<std::setw(12)<< data(i,j)<< ' ';
		cout<<'\n';
	}
}
/// print 3d vec
void print_vec3d(Array3D<double> data){
    int nd1=data.XSize();
	int nd2= data.YSize();
    int nd3= data.ZSize();
    cout<<nd1<<' '<<nd2<<' '<< nd3<< '\n';
    for(int i=0; i<nd1; i++){
		for(int j=0; j<nd2;j++){
            for(int k=0; k<nd3; k++){
                cout<< data(i,j,k)<< ' ';
                }
         cout<<'\n';}
    }
    cout<<endl<<endl;
}

/// sum(mi)
double sum_vec(Array1D<double> mi){
	int nd=mi.XSize();
	double sm=0;
	for(int i=0; i<nd; i++)
		sm+=mi(i);
	return sm;
}

/// sum(mi)
int sum_vec(Array1D<int> mi){
	int nd=mi.XSize();
	int sm=0;
	for(int i=0; i<nd; i++)
		sm+=mi(i);
	return sm;
}

/// long choose(K+N,N)
long computenpc(int ndim,int norder)
{
  if (norder==-1)
    return 0;

  long enume=1;
  long denom=1;

  int minNO = min(norder,ndim);

  for(int k=0; k < minNO; k++){
      enume = enume*(norder+ndim-k);
      denom = denom*(k+1);
  }

  long nPCTerms = enume/denom;

  return nPCTerms;
}

// modified sandia UQK implementation: int changed to long
int compute_multi_ind(int ndim,int norder, Array2D<int>& mi)
{
  if (ndim==0) return 1;

  // Compute the number of PC terms
  long npc=computenpc(ndim,norder); // this was int

  // Initialize multiIndex
  int iup=0;
  int isum=0;

  // Work arrays
  Array1D<int> ic(ndim,0);
  Array1D<int> ict(ndim,0);

  mi.Resize(npc,ndim,0);


  //-----------zero-th order term-------------------------
  iup=0;
  for(int idim=0;idim < ndim;idim++){
    mi(iup,idim)=0;
  }
  if(norder > 0){
  //-----------first order terms---------------------------
    for(int idim=0;idim < ndim;idim++){
      iup++;
      mi(iup,idim)=1; //multiIndex is a kronecker delta
      ic(idim)=1;
    }
  }
  if(norder > 1){
  //-----------higher order terms--------------------------
    for(int iord=2;iord<norder+1;iord++){
      int lessiord=iup;                   //number of terms of order less than iord
      for(int idim=0;idim<ndim;idim++){
        isum=0;
        for(int ii=idim;ii<ndim;ii++){
          isum=isum+ic(ii);
        }
        ict(idim)=isum;
      }

      for(int idim=0;idim < ndim;idim++){
        ic(idim)=ict(idim);
      }

      for(int idimm=0;idimm<ndim;idimm++){
        for(int ii=lessiord-ic(idimm)+1;ii<lessiord+1;ii++){
          iup++;
          for(int idim=0;idim < ndim;idim++){
            mi(iup,idim)=mi(ii,idim);
          }
          mi(iup,idimm)=mi(iup,idimm)+1;
        }
      }
    }
  }

  return npc;
}

double dot(Array1D<double> arr1, Array1D<double> arr2)
{
    unsigned int nd = arr1.XSize();
    double sm=0;
    assert(nd==arr2.XSize());

    for(unsigned int i=0; i<nd; ++i)
        sm+=arr1(i)*arr2(i);

    return sm;
}

/// sum(sum(arr1.*arr2))
double dot2d(Array2D<double> arr1, Array2D<double> arr2)
{
    unsigned int nd1 = arr1.XSize();
    unsigned int nd2 = arr1.YSize();
    double sm=0;
    assert(nd1==arr2.XSize());
    assert(nd2==arr2.YSize());

    for(unsigned int i=0; i<nd1; ++i)
        for(unsigned int j=0; j<nd2; ++j)
                sm+=arr1(i,j)*arr2(i,j);

    return sm;
}

// (starts from index 1)
double cal_var(Array1D<double> uend, int npc){
    double sm=0.0;
    for(int i=1; i<npc; i++)
        sm+= pow(uend(i),2);
    return sm;
}

/// calculates cosines in the definition of White noise
double cal_mif(int k, double t, double Ti, double Tf)
{   // orthonormal basis for L2[Ti,Tf]
    if (k==1) {
        return 1.0/sqrt(Tf-Ti);
    }else{
        return sqrt(2.0/(Tf-Ti))*cos((k-1)*M_PI*(t-Ti)/(Tf-Ti));
    }
}

/// calculates sines in definition of Brownian motion (Karhunen-Loeve)
double cal_sif(int i, double s, double Ti, double Tf)
{
    if(i==1){
        return (s-Ti)/(sqrt(Tf-Ti));
    }
    else{
        return (sqrt(2.0*(Tf-Ti))/((i-1)*M_PI))*sin((i-1)*M_PI*(s-Ti)/(Tf-Ti));
    }
}

void fold_1dto2d(Array1D<double>& x1, Array2D<double>& x2)
{
    int nx=x2.XSize();
    int ny=x2.YSize();
    int nxy=nx*ny;

    if (nxy != (int) x1.XSize()) {printf("fold_1dto2d: dimension error\n"); exit(1);}

    for(int i=0;i<nx;i++){
        for(int j=0;j<ny;j++){
        x2(i,j)=x1(j*nx+i);
        }
    }
    return;
}

/// \todo faster
Array1D<int> getRowmod(Array2D<int>& arr2d, int k)
{
    Array1D<int> arr1d;
    for(int i=0;i<(int)arr2d.YSize();i++)
        arr1d.PushBack(arr2d(k,i));

    return arr1d;
}

Array1D<double> getRowmod(Array2D<double>& arr2d, int k)
{
    Array1D<double> arr1d;
    for(int i=0;i<(int)arr2d.YSize();i++)
        arr1d.PushBack(arr2d(k,i));

    return arr1d;
}

Array1D<unsigned int> getRowmod(Array2D<unsigned int>& arr2d, int k)
{
    Array1D<unsigned int> arr1d;
    for(int i=0;i<(int)arr2d.YSize();i++)
        arr1d.PushBack(arr2d(k,i));
    
    return arr1d;
}

/// row_k(1:K) row k first K elements
Array1D<int> getRowtop(Array2D<int>& arr2d, int k,int K)
{
    assert(K<(int)arr2d.YSize());
    Array1D<int> arr1d;
    for(int i=0;i<K;i++)
        arr1d.PushBack(arr2d(k,i));

    return arr1d;
}

/// row_k(K1:K1+K2-1) row_k : K2 elements starting from K1
Array1D<int> getRowmiddle(Array2D<int>& arr2d, int k, int K1, int K2)
{
    assert(K2<(int)arr2d.YSize());
    Array1D<int> arr1d;
    for(int i=K1;i<K1+K2;i++)
        arr1d.PushBack(arr2d(k,i));

    return arr1d;
}

/// row_k(end-D:end) row k last D elements
Array1D<int> getRowtail(Array2D<int>& arr2d, int k,int D)
{
    int ny = (int)arr2d.YSize();
    assert(D<ny);
    Array1D<int> arr1d;
    for(int i=ny-D;i<ny;i++)
        arr1d.PushBack(arr2d(k,i));

    return arr1d;
}

Array1D<double> getColmod(Array2D<double>& arr2d, int k)
{
    Array1D<double> arr1d;
    for(int i=0;i<(int)arr2d.XSize();i++)
        arr1d.PushBack(arr2d(i,k));

    return arr1d;
}


/// prod(vector)
unsigned long long  prod(Array1D<unsigned long long> a){
	int sz=a.XSize();
	int prod=1,i;
	/* //slow
	int *p;
	int a_length = sizeof(a)/sizeof(a(0));
	int *end = a.GetArrayPointer()+a_length;
	for (p = a; p!=end; ++p) {
		prod *= *p;
	} */
	for(i=0; i<sz; i++ )
	   prod*= a(i);
	 return prod;
}

void find_ind(Array1D<int>& theta, int lambda, string type, Array1D<int>& indx)
{
    indx.Clear();
    if ( type == "gt" )
    {
        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) > lambda ) indx.PushBack(i) ;
        return ;
    }
    if ( type == "ge" )
    {

        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) >= lambda ) indx.PushBack(i) ;
        return ;
    }
    if ( type == "lt" )
    {
        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) < lambda ) indx.PushBack(i) ;
        return ;
    }
    if ( type == "le" )
    {
        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) <= lambda ) indx.PushBack(i) ;
        return ;
    }
    if ( type == "eq" )
    {
        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) == lambda ) indx.PushBack(i) ;
        return ;
    }
    if ( type == "neq" )
    {
        for ( int i = 0; i<(int) theta.XSize(); i++)
            if ( theta(i) != lambda ) indx.PushBack(i) ;
        return ;
    }

    return ;
}


Array1D<unsigned long long> vec_fact(Array1D<int> a){

	int sz=a.XSize();
	Array1D<unsigned long long> temp(sz);

	for(int i=0; i<sz;i++)
		temp(i) = static_cast< unsigned long long > (boost::math::factorial<double>(a(i)));
       
	return temp;
}


int check_all(Array1D< int> mi){
	int ret=0;
	int K=mi.XSize();
	for(int i=0; i<K;i++)
		if(mi(i)<0) ret=1;
	return ret;
}

double multiind_choose(Array1D<int> a, Array1D<int> b){
    // choose(a,b) where a,b multi indices
     int K=a.XSize();
     double sm=1;
     Array1D<int> tmp(K,0);
     tmp = a-b; //writes to a \todo fix 
     a+b; /// inplace addition

     //  a!/(a-b)!*b!
     for(int i=0; i<K; ++i){
        sm*= exp( lgamma(a(i)+1)  - lgamma(tmp(i)+1) - lgamma(b(i)+1) );
     }

     return sm;
 }


double cal_C(Array1D< int> a,Array1D< int> b,Array1D< int> c ){
    Array1D<int> temp(a.XSize());
	temp=a-b;
	a+b;

	if (fabs(check_all(temp)-0)<1e-13){

        double a1,a2,a3;

        a1= multiind_choose(a,b);
        a2= multiind_choose(b+c,c); //b=b+c now
        a3= multiind_choose(temp+c,c);

        return exp(0.5*(log(a1)+log(a2)+log(a3))); //sqrt(a1 a2 a3)
	}
	else{
	    cout<<"a-b>=0"<<endl;
        throw "Negative Values"; // check negative values.
	}


}

/// C(j,l,m) for scalar product formula
double cal_C_int(int j,int l,int m){
	return sqrt( (double) nChoosek(j, l) * nChoosek(l + m, m) * nChoosek(j - l + m, m));
}

/// Ex: fourth order poly: [1*x^4 + Fu(4,1) x^3 + Fu(4,2)x^2  +Fu(4,3) x + Fu(4,4)]
void cal_orth_coeff_Fu(Array1D<double> mom_vec, int L, Array2D<double>& Fu){
    // moments=[1.0 E[x] E[x^2]   ....E[x^2n]]
    // number of moments are 2L
    // A = zeros(L+1,L+1)
    // a = np.zeros((L + 1, L + 1)) # -un normalized coeff a^i_k = A_ki/A_kk
    double sm;
    Array2D<double> A(L+1,L+1), a(L+1,L+1);

    for(int i=0; i< L+1 ; i++){
        A(0,i) = mom_vec(i);
        a(i,0) = 1.0;
    }

    for(int k=1; k<L+1;k++){
        for(int i=1; i<L+1; i++ ){
            sm=0.0;
            for(int j=0; j<k; j++)
                sm+= (double)(A(j,k)*A(j,i))/(double)A(j,j); 

            A(k,i)= mom_vec(i+k) - sm;
        }
    }

    a(0,0) = A(0,1); 
    for(int i=1; i<L+1;i++){
        for(int k=1; k<i+1; k++ ){
            if(i==k){
                a(i,k)=mom_vec(i);
            }else{
                a(i, k) = A(i - k, i)/ A(i - k, i - k);
            }
        }
    }

    // Fu= monomials
    Array1D<double> temp(L+1);
    for(int i=0; i<L+1; i++) 
        for(int j=0; j<L+1; j++)
            Fu(i,j) = - a(i,j); //make them negative (a stored as positives)

    for(int i=2; i<L+1; i++){
        temp=getRowmod(Fu,i);
        for(int j=2; j< i+1; j++)
            for(int k=j; k<i+1; k++)
                Fu(i,k) += temp(j-1)*Fu(i-j+1,k-(j-1));
    }

    for(int i=0; i<L+1; i++)
        Fu(i,0) = - Fu(i,0);

    //normalization
    Fu(0,0) =1.0;
    for(int k=1; k<L+1; k++ ){
        if(fabs(A(k,k)) > 1E-15)
            for(int i=0; i<L+1; i++)
                Fu(k,i) /= sqrt(fabs(A(k,k)));
    }
}

Array1D<double> power(Array1D<double> X, int p){
    int nd= X.XSize();
    Array1D<double> Y(nd,0);
    for(int i=0; i< nd; i++)
        Y(i) = pow(X(i),p);
    return Y;
}


int nonzero(Array1D<int> mi){
	int K=mi.XSize();
	for(int i=0; i<K;i++)
		if(mi(i)!=0)
			return i;
	return 0;
}



Array2D<int> weight_mat(int K,int N){
	Array2D<int> w_mat(N,K);
	int i,j;
	for(j=1; j<K+1;j++){
		for(i=1;i<N+1;i++){
			w_mat(i-1,j-1) = nChoosek(K-j+i-1,K-j); 
		}
	}
	for(j=1; j<K;j++)
		for(i=0;i<N;i++)
        w_mat(i, j) = w_mat(i, j) + w_mat(i, j - 1);

	return w_mat;
}

int ret_index(Array1D<int> mi, Array2D<int> w_mat){
	// w_mat : weight mat keeps the weights to compute the index
	int cnt= sum_vec(mi);
	int N = w_mat.XSize();
	int K= w_mat.YSize();
	int ret;

	if (cnt > N || check_all(mi)) return -1;

	if(cnt==0)
		return 0;
	else if(cnt==1)
        return nonzero(mi)+1;
	else{
		ret=0;
		for(int i=0; i<K; i++){
			while(mi(i)>0){
				ret += w_mat(cnt-1,i);
				mi(i) -= 1;
				cnt-=1;
			}
			if(sum_vec(mi)==0) break; 
		}
	}

	return ret;
}
//sparse
int ret_indexsp(Array1D<int> mi, Array2D<int> w_mat, int ncu){
    // w_mat : weight mat keeps the weights to compute the index
    
	int cnt= sum_vec(mi);
	int N = w_mat.XSize();
	int K= w_mat.YSize();
	int ret;

	if (cnt > N || check_all(mi)) return ncu; //this is different part

	if(cnt==0)
		return 0;
	else if(cnt==1)
        return nonzero(mi)+1;
	else{
		ret=0;
		for(int i=0; i<K; i++){
			while(mi(i)>0){
				ret += w_mat(cnt-1,i);
				mi(i) -= 1;
				cnt-=1;
			}
			if(sum_vec(mi)==0) break;  
		}
	}

	return ret;
}

// mu= mean, sigma=std deviation
Array1D<double>  gaussian_mom(double mu,double sigma, int M){ // mu= mean, sigma=std deviation
    Array1D<double> mom_vec(M+1);
    mom_vec(0)=1;  // adjust the index
    mom_vec(1) = mu;
    mom_vec(2) = pow(mu,2)+ pow(sigma,2);

    for(int n=3; n<M+1; n++)
        for(int p=0; p<floor((double)n/2.0)+1; p++){
            mom_vec(n) +=  nChoosek(n,2*p) * double_factorial(2*p-1) * pow(sigma,(2*p)) * pow(mu,(n-2*p));}

    return mom_vec;
}

Array1D<double> find_cumulants(Array1D<double>& mom_vec){

    int sz= mom_vec.XSize();
    double sm;
    Array1D<double> cum_vec(sz);

    cum_vec(0) = 1; //array purposes;
    cum_vec(1) = mom_vec(1); // mean

    for(int n=2; n<sz; ++n){
        sm=0;
        for(int m=1; m<n; ++m)
            sm+= nChoosek(n-1,m-1) * cum_vec(m)*mom_vec(n-m);

        cum_vec(n) = mom_vec(n) - sm;

    }

    return cum_vec;
}

void flip_basis(Array2D<double>& Fu){
    int sz= Fu.XSize();
    for(int i=0; i<sz; ++i){
        for(int j=i; j<sz;++j ){
            Fu(i,j)= Fu(j,j-i);
            if(i>0)
                Fu(j,j-i)=0;
        }
    }
}


double computeLambda(int p){
    return exp(lgamma(2*p+1) - p*log(2) - 2*lgamma(p+1));
}

// Normalized Legendre
double legendre_trip(int n, int p, int q ){

    int s1= p+q-n, s2= n+q-p, s3= n-q+p;
    
    if( s1<0 || s2<0 || s3<0 || s1%2 ==1 || s2%2==1 || s3%2==1){
        //negative or odd
        return 0;
    }else{
        return  sqrt((2*p+1)*(2*n+1)*(2*q+1))* 1/((double)(n+p+q+1))
                        * (computeLambda(s1/2)*computeLambda(s2/2)*computeLambda(s3/2))/computeLambda((n+p+q)/2);
    }

}
/// not normalized Hermite
double hermite_trip_int(  int i,  int j,  int k){
    /// exp(log(gamma)) implementation
    int s= (i+j+k)/2;

    if(s<i || s<j || s<k || (i+j+k)%2==1){
        return 0.0;
    }else{
        //  i!j!k! /((s-i)! (s-j)! (s-k)!)
        return exp(lgamma(i+1)+lgamma(j+1)+lgamma(k+1)- lgamma(s-i+1)-lgamma(s-j+1)- lgamma(s-k+1));
    }
}

