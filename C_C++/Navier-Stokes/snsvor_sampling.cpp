#include "snsvor_sampling.h"
\




void MomentsFromData(int D, int Nsamp, int npcue3, Array2D<int>& mindexue3,
                     MatrixXd& EtaSamples, VectorXd& MeansEta, MatrixXd& MomentsMatEta)
{
    MeansEta.setZero();

    MeansEta(0) =1;
    Array1D<int> ind(D),temp;

    double sm, sm1;

    int j,k,nnz;

    for(int i=1; i<npcue3; ++i){
        ind= getRowtail(mindexue3,i,D);
        find_ind(ind,0,"neq",temp);

        nnz=temp.XSize();
        if(nnz==0){
            sm=1;
        }else if(nnz==1){
            sm=0;
            for(j=0; j<Nsamp; ++j)
                sm+= pow(EtaSamples(j,temp(0)), ind(temp(0)))/(double)Nsamp;

            MomentsMatEta(ind(temp(0)),temp(0)) = sm;
        }else{
            sm=0;
            for(j=0; j<Nsamp; ++j){
                sm1=1;
                for(k=0; k<D; ++k)
                    sm1*= pow(EtaSamples(j,k), ind(k));

                sm+= sm1/(double)Nsamp;
            }
        }
        MeansEta(i) = sm;
    }
}

void EvalHermite1d(VectorXd& xi, MatrixXd& basis,int N)
{
    basis.setOnes();

    if(N>0){
       basis.col(1) = xi;
       for(int i=2; i<N+1; ++i){
        basis.col(i) = xi.cwiseProduct(basis.col(i-1)) - (i-1)*basis.col(i-2);
       }
    }

}

void EvalMultiHermite(int K ,int N, Array2D<int>& mindex, VectorXd& xi, VectorXd& Psi)
{
    MatrixXd basis(xi.size(),N+1);
    int npc = mindex.XSize();
    assert(xi.size()==K);

    EvalHermite1d(xi, basis,N);

    Psi.setOnes();
    for(int i=0; i<K; ++i){
        for(int j=0; j< npc; ++j )
           Psi(j)*=basis(i,mindex(j,i));
    }

}

void GetSamplesEta(const int K,const int N, Array2D<int>& mindex, MatrixXd& EtaKLModes, MatrixXd& EtaSamples)
{

    int Nsamp=EtaSamples.rows();
    VectorXd xi(K);

    int npc= EtaKLModes.rows();
    VectorXd Psi(npc);

    boost::mt19937 rng;
    boost::normal_distribution<> nd(0.0, 1.0);
    rng.seed(static_cast<unsigned int>(std::time(0)+getpid()));
    boost::variate_generator<boost::mt19937&,
                           boost::normal_distribution<> > var_nor(rng, nd);


    EtaSamples.setZero();
    int D=EtaKLModes.cols();


   for(int i=0; i<Nsamp; i++){
            for(int id=0; id<K; id++)
                xi(id) =  var_nor();

         EvalMultiHermite(K,N,mindex,xi,Psi);

         for(int j=0; j<D; ++j)
            for(int ipc=0; ipc< npc; ipc++)
               EtaSamples(i,j) += EtaKLModes(ipc,j)*Psi(ipc); //np.sum(pce*Psi);

    }
}

void GetSamplesEtaAfter(const int K,const int D, Array2D<int>& mindexue, MatrixXd& EtaKLModes,
                MatrixXd& EtaSamplesOld, MatrixXd& OrthPolyMat, MatrixXd& EtaSamplesNew)
{

    int Nsamp=EtaSamplesNew.rows();
    VectorXd xi(K),xi2(D);
    int npc= mindexue.XSize();

    boost::mt19937 rng;
    boost::normal_distribution<> nd(0.0, 1.0);
    rng.seed(static_cast<unsigned int>(std::time(0)+getpid()));
    boost::variate_generator<boost::mt19937&,
                           boost::normal_distribution<> > var_nor(rng, nd);


    EtaSamplesNew.setZero();
    VectorXd xieval(npc),basiseval(npc);

    for(int i=0; i<Nsamp;++i){
            for(int id=0; id<K; id++)
                xi(id) =  var_nor();

        xi2= EtaSamplesOld.row(i);
        eval_xiboth(K,D,xi,xi2,mindexue,xieval);

        basiseval.setZero();
        for(int p=0; p< npc; p++)
            for(int pl=0; pl<p+1;++pl)
                basiseval(p)+= OrthPolyMat(p,pl) * xieval(pl);

        for(int j=0; j<D; ++j){
            for(int p=0; p< npc; p++){
                EtaSamplesNew(i,j) += EtaKLModes(p, j)*basiseval(p);
            }
        }

    }

}

void eval_xiboth(int K,int D,VectorXd& xi,VectorXd& xi2,Array2D<int>& mindexue,VectorXd& xieval)
{
    for(int i=0; i< (int) mindexue.XSize();++i)
        xieval(i) = pwind(xi,getRowtop(mindexue,i,K))*pwind(xi2,getRowtail(mindexue,i,D));
}

double pwind(VectorXd& x, Array1D<int> ind)
{

 double sm=1;
 for(int i=0; i< (int)ind.XSize();++i)
    sm*= pow(x(i), ind(i));

  return sm;
}


void EvalLegendre1d(VectorXd& xi, MatrixXd& basisEvals, int N )
{
   // basisEvals.resize(xi.size(),N+1);
    VectorXd ca;
    double cb;
    basisEvals.setOnes();
    if(N > 0){
      basisEvals.col(1) = xi*sqrt(3.0);
      for(int iord=2; iord < N+1; iord++){
        ca = sqrt(2.e0*(double)(iord-1) + 1.e0)*xi;
        cb = (double)(iord-1)/sqrt(2.e0*(double)(iord-2) + 1.e0);
        basisEvals.col(iord) = sqrt(2.e0*(double)(iord) + 1.e0)*(ca.cwiseProduct(basisEvals.col(iord-1))
                        - cb*basisEvals.col(iord-2))/(double)(iord);
      }
    }

}

void evalMultiHermiteLeg(int K ,int N, Array2D<int>& mindex, VectorXd& xi, VectorXd& U,VectorXd& Psi)
{

    MatrixXd her_basis(xi.size(),N+1), leg_basis(U.size(),N+1);

    int npc = mindex.XSize();
    assert(xi.size()==K);
    int Kall  = mindex.YSize();
    assert(U.size()==Kall-K);

    EvalHermite1d(xi, her_basis,N);
    EvalLegendre1d(U,leg_basis,N);

    Psi.setOnes();
    for(int j=0; j< npc; ++j )
        for(int i=0; i<K; ++i)
           Psi(j)*=her_basis(i,mindex(j,i));


    for(int j=0; j<npc; ++j)
        for(int l=K; l<Kall; ++l)
            Psi(j) *= leg_basis(l-K,mindex(j,l));

}

void GetSamplesEtaUniform(const int K,const int N, Array2D<int>& mindex, MatrixXd& EtaKLModes, MatrixXd& EtaSamples)
{

    int Nsamp=EtaSamples.rows();
    VectorXd xi(K);
    int Kall=mindex.YSize();

    int npc= EtaKLModes.rows();
    VectorXd Psi(npc);

    boost::mt19937 rng;
    boost::normal_distribution<> nd(0.0, 1.0);
    boost::uniform_real<> ud(-1, 1);

    rng.seed(static_cast<unsigned int>(std::time(0)+getpid()));

    boost::variate_generator<boost::mt19937&,
                           boost::normal_distribution<> > var_nor(rng, nd);
    boost::variate_generator<boost::mt19937&,
                           boost::uniform_real<> > unif(rng, ud);


    EtaSamples.setZero();
    int D=EtaKLModes.cols();
    VectorXd U(Kall-K);

   for(int i=0; i<Nsamp; i++){
        for(int id=0; id<K; id++)
            xi(id) =  var_nor();
         for(int id=0; id<Kall-K;++id)
            U(id) =  unif();

         evalMultiHermiteLeg(K , N, mindex, xi, U,Psi);

         for(int j=0; j<D; ++j)
            for(int ipc=0; ipc< npc; ipc++)
               EtaSamples(i,j) += EtaKLModes(ipc,j)*Psi(ipc); //np.sum(pce*Psi);

    }

}

