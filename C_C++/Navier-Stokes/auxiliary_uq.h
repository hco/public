
#ifndef AUXILIARY_H
#define AUXILIARY_H


#include "Array1D.h"
#include "Array2D.h"
#include "Array3D.h"

#include <iomanip>
#include <iostream>

#include <boost/math/special_functions/factorials.hpp>
#include "boost/random.hpp"
#include "boost/random/normal_distribution.hpp"
#include <boost/math/special_functions/fpclassify.hpp> // for isnan
#include <math.h>


/* Simple general routines for PCE and array manipulations
 * e.g. multi-index set, factorial computations, orthogonal poly.,
 * array manipulations, triple products
 * 
 * H. Cagan Ozen, 2013-2014
 */


/// dynamic 1d double array
typedef std::vector<double> Vector1D;
/// dynamic 1d unsigned shorts array
typedef std::vector<unsigned short> Vector1Dusint;
/// dynamic array where each element is Array1D<int>
typedef std::vector<Array1D<int> > Vector2Dint;
/// dynamic array where each element is matrix Array2D<int>
typedef std::vector<Array2D<int> > Vector3Dint;


/// choose(n,k))
unsigned long nChoosek( unsigned , unsigned );

/// double factorial
unsigned long double_factorial(int );

/// print 1d vec
void print_vec(Array1D<double> );

/// print 2d vec
void print_vec2d(Array2D<double> );

/// print 3d vec
void print_vec3d(Array3D<double> );

/// sum(1d array<double>)
double sum_vec(Array1D<double> );

/// sum(1d array<int>)
int sum_vec(Array1D<int> );

///sandia implementation of choose(K+N,N): the number of terms in the expansion
long computenpc(int ,int );

/// compute multi index set
int compute_multi_ind(int ,int , Array2D<int>& );

/// dot product of two arrays
double dot(Array1D<double> , Array1D<double>);

/// sum(sum(arr1.*arr2)
double dot2d(Array2D<double> , Array2D<double> );

/// compute variance of 1d pce  
double cal_var(Array1D<double> , int );

/// calculates cosines in definition of White  noise
/// orthonormal basis for L2[Ti,Tf]
double cal_mif(int , double , double , double ); 

/// calculates sines in definition of Brownian motion (Karhunen Loeve)
double cal_sif(int , double , double , double );

/// fold 1d to 2d column-wise fashion
void fold_1dto2d(Array1D<double>& , Array2D<double>& );

/// get row k from 2d array
/// \todo faster implementation
Array1D<int> getRowmod(Array2D<int>& , int );
Array1D<unsigned int> getRowmod(Array2D<unsigned int>& , int );
Array1D<double> getRowmod(Array2D<double>& , int );
/// row_k(1:K) row k first K elements
Array1D<int> getRowtop(Array2D<int>&, int ,int) ;
/// row_k(K1:K1+K2-1) row_k : K2 elements starting from K1
Array1D<int> getRowmiddle(Array2D<int>& , int , int, int );
/// row_k(end-D:end) row k last D elements
Array1D<int> getRowtail(Array2D<int>&, int ,int );
/// col_k = A(:,k)
Array1D<double> getColmod(Array2D<double>&, int );

/// prod(vector)
unsigned long long prod(Array1D<unsigned long long> );

///(sandia) linear search for elements, push_backs required indices of elems in a vector
void find_ind(Array1D<int>& , int , string , Array1D<int>& );

/// alpha! for multiindex alpha
Array1D<unsigned long long> vec_fact(Array1D<int> );

/// check whether nagative value occurs
int check_all(Array1D<int> );

///  exp(log(gamma)) implementation of choose(a,b) where a,b multi indices
double multiind_choose(Array1D<int> , Array1D<int> );

/// Ca,b,c) ceofficient for multi-index Hermite product formula
double cal_C(Array1D< int> ,Array1D< int> ,Array1D< int>  );

/// C(a,b,c) for scalar Hermite product formula
double cal_C_int(int ,int ,int );

/// 1d orthonormal polynomials Fu from moments (linear combination of monomials)
void cal_orth_coeff_Fu(Array1D<double> , int , Array2D<double>& );

/// X.^p of a given array X
Array1D<double> power(Array1D<double> , int );

//return the first nonzero index in mi
int nonzero(Array1D<int> );

/// auxiliary weight matrix to be used to calculate rank of multiindices
/// first input K dimension, the second input N degree
Array2D<int> weight_mat(int ,int );

/// return the rank of a multi-index in the multi-index set 
int ret_index(Array1D<int> mi,Array2D<int> );
/// return the rank of a multi-index in the Sparse multi-index set 
int ret_indexsp(Array1D<int> ,Array2D<int> , int );

/// gaussian moments returns a matrix
Array1D<double>  gaussian_mom(double ,double , int );

/// 1d cumulants given moments
Array1D<double> find_cumulants(Array1D<double>& );

/// flips orth. poly. matrix
void flip_basis(Array2D<double>& );

// subroutine for Legendre triple products
double computeLambda(int );
/// 1d NORMALIZED Legendre poly triple products E[T_k T_l T_m ]
double legendre_trip(int , int , int );
/// 1D Hermite polynomials triple products E[T_k T_l T_m ]
/// not normalized
double hermite_trip_int(  int ,  int ,  int );

#endif // AUXILIARY_H
