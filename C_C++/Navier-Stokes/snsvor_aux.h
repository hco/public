#ifndef SNSVOR_AUX_H
#define SNSVOR_AUX_H

#include "auxiliary_uq.h"
#include <math.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/ArpackSupport>
#include <unsupported/Eigen/FFT>
#include <fftw3.h>

#include <boost/progress.hpp>

using namespace std;
using namespace Eigen;

/*
 * necessary subroutines for DgPC
 * H. Cagan Ozen, June 2015
 */


#define MM 64 // \todo


// -------templates for necessary data structures----

// fixed MM x MM double matrix
typedef Matrix<double, MM, MM> MatMM;
// dynamic array of each element is MatMM
typedef Matrix<MatMM,Dynamic,Dynamic> ArrMatXd;
// fixed MM x MM complex matrix
typedef Matrix<std::complex<double>, MM,MM> MatcMM;
// dynamic array of each element is MatcMM
typedef Matrix<MatcMM,Dynamic,Dynamic> ArrMatXcd;
// dynamic vector of integer vectors vectorXi
typedef std::vector<VectorXi > Vector2Dxi ;
// dynamic vector of Matrix doubles
typedef std::vector<MatrixXd > Vector3Dxd ;
// triplets of complex doubles
typedef Eigen::Triplet<std::complex<double> > triplet;
// dynamic array of triplets
typedef std::vector<triplet> tripletlist;

/// initializes double ArrMat as zero
void init_arrmat(ArrMatXd& , int , int );
/// initializes complex ArrMat as zero
void init_arrmat(ArrMatXcd& , int, int);
/// copy ArrMat
void copy_arrmat(ArrMatXd& ,  int , int , ArrMatXd& );

// -------END templates for necessary data structures----


// ----------FFT functions-------

/// set one-dimensional frequencies in FFT
void FFT1dFreqVector(int ,VectorXd& );
/// copy double matrix into fft complex out
void eigenxd_to_fftw(MatrixXd& , fftw_complex*,int , int );
/// copy complex matrix into fft complex matrix
void eigenxcd_to_fftw(MatrixXcd& , fftw_complex *,int, int);
/// copy fft complex matrix into complex matrix
void fftw_to_eigenxcd(fftw_complex*, MatrixXcd&,int, int);
/// take complex fft matrix copies it into real double matrix and scales the FFT2
void fftinv_to_eigenxd(fftw_complex*, MatrixXd&,int , int);
/// takes eigen real Matrix copies to fft in-- take fft -- write into eigen matrix
void fft2_eigenxdc(MatrixXd& ,int ,fftw_plan& ,fftw_complex* ,fftw_complex*,MatrixXcd&);
/// takes complex matrix  copy --inverse fft --write back
void ifft2_eigenxcmatd(MatrixXcd& ,int ,fftw_plan& , fftw_complex* , fftw_complex* ,MatrixXd& );
/// take fft complex matrix copies into ArrMat(indx,indy) (real matrix)
void fftinv_to_arrmatxd(fftw_complex* , ArrMatXd&,int , int,int, int);
/// copy eigen complex matrix -- take inverse fft2 -- write into ArrMat(indx,indy)
void ifft2_eigenxc_arrmatd(MatrixXcd& ,int,fftw_plan&, fftw_complex*, fftw_complex*,ArrMatXd&,int,int);

// ----------END FFT functions-------


// ----------Sparse multi-index routines-------

/// set sparse multi-index given  values of input parameters
/// returns last two arguments
void SetSparseIndex(const int, const int, const int, const int, const int, MatrixXi&, MatrixXi& );
/// copy eigen matrix<int> into array2d<int>
void eigentouqcopy(MatrixXi& ,Array2D<int>&);
/// add two sparse vector matrix for each degrees of freedom up to 3N
void fill_rmat(Vector3Dint& , Array2D<int>& ,int );
/// compute partitions of an integer into 2
void IntegerPartition(int , Vector2Dxi& );
/// adds two sparse multi-index vector
Array2D<int> radd(Array2D<int>& , Array2D<int>& );
/// computes sparse multi-index set from full multi-index set
/// with the aid of sparse r vector
void multi_ind_mat_sp(Array2D<int>& ,int ,Array2D<int>& ,Array2D<int>& , Array1D<int>& );

// ----------END Sparse multi-index routines-------


/// Solve Laplace's equation with the right side rhs in frequency domain
/// psi is the stream function
void SolveLaplace(int,MatrixXd&,MatrixXcd&,MatrixXcd& );
/// compute moments of gaussian(nu,sigma)
void gaussian_mom(double ,double , int , VectorXd& );
/// computes multi-index moments from one dimensional moments for first K variables
/// in the multi-index set (corresponding to Gaussian random variables)
void get_multimeans_xi(Array2D<int>& ,int ,VectorXd& ,VectorXd&);
/// computes triples products of multi- Gaussian random variables in sparse format, 
void trip_prod_wick_sp(Vector1D& , Array2D<int>& , Vector2Dint&);
/// Pre-computation of indices used in Triple Products during Offline Step
void trip_prod_cmbnd_preind(int , Array2D<int>& ,Array2D<int>& ,Array1D<int>& ,Vector1Dusint& );


/// computes exp(vis*k^2*t(i)) matrix at time t(i)
void exp_mat(int , double , MatrixXd& ,double ,MatrixXd& );
/// sum(input.^2)
MatrixXd sum2_arrmat(ArrMatXd& ,int ,int );


/// multiplies two PC expansions u & v (ArrMat) and writes to uv using triple products
void ProductTwoPCEs(int , int , ArrMatXd& ,ArrMatXd& , ArrMatXd& ,
                      Vector1D& , Vector2Dint& , int );

/// Takes FFT2 of given PCEs In1 & In2 writes to Out1 Out2
void FFT2_AllPCEs(int , int , ArrMatXd& , ArrMatXd& ,   MatrixXd& , MatrixXcd& , fftw_plan& , fftw_complex *,fftw_complex *,
                    ArrMatXcd& , ArrMatXcd& );
                    
/// takes FFT of Vorticity- take inverse FFT- solve laplacian- calculate velocity components (u,v)
void IFFT2_ComputeVelocity(int , int , MatrixXd& , MatrixXcd& ,ArrMatXcd& , ArrMatXd& ,    MatrixXcd& ,  MatrixXcd& ,
                MatrixXcd& ,MatrixXd& ,fftw_plan& ,fftw_plan& ,fftw_complex *,fftw_complex *,fftw_complex *, ArrMatXd& , ArrMatXd& );

/// evaluates right hand side of SNS with integrating diffusion term exactly
void EvalRhsUsingExp(double ,double , double ,MatrixXd& , int ,int , int ,MatrixXcd& ,MatrixXcd& ,ArrMatXcd& ,ArrMatXcd& ,
                MatrixXcd& ,MatrixXcd& , ArrMatXcd& );
                
/// one step of Runge_Kutta4 to initialize Predictor-Corrector4
void one_step_RK4(double ,double , double , double , double ,int ,int , int , int ,
                 ArrMatXcd& , ArrMatXcd& , MatrixXcd& , MatrixXcd& ,MatrixXd& ,
                 MatrixXcd& ,MatrixXcd& , Vector1D& , Vector2Dint& , int ,
                 fftw_plan& ,fftw_plan& ,fftw_complex *,fftw_complex *,fftw_complex *,ArrMatXcd& );

/// Integration of odes resulting from PCE of SNS with determistic viscosity
/// exponential integrator scheme is used for diffusion term
/// 4th order Adams Predictor-Corrector scheme
void ODEint_SNS_DetVis_PC4(double ,double ,int ,int , int ,int ,
                int , double , MatrixXcd& ,  MatrixXcd& ,MatrixXd& ,MatrixXcd& ,MatrixXcd& ,
                ArrMatXd& , Vector1D& , Vector2Dint& , int , ArrMatXd& ,ArrMatXd& ,
                fftw_plan& , fftw_plan& ,fftw_complex *,fftw_complex *,fftw_complex *, ArrMatXd& );

/// compute centered 3rd and 4th moment of a given PCE 
void SetHigherMoments(ArrMatXd&  ,int ,int ,int ,Vector1D& ,Vector2Dint& ,int , ArrMatXd& , ArrMatXd& );


// -------- Eigenvalue problem, dimensionality reduction Karhunen-Loeve-----------



/// normalize Eigenfunction ||.||_2 =1
void NormalizeEigenfunc(ArrMatXd&,int , int );

/// real version of eigenvalue solver 
void CovarianceEigenSolver(int ,int , SparseMatrix<float>& , string , ArrMatXd& ,VectorXd&  );
/// computes Eigenfunctions and Eigenvalues using Covariance in real domain
/// first few dominating eigenvalues and eigenfunctions will be returned
/// Arpack computes few first Eigenvalues and EigenFunctions
void GetCovarianceEigenfunc(ArrMatXd& , int , int ,int ,string ,ArrMatXd& ,VectorXd& );
/// computes Covariance in real domain, spatial covariance (M^2 x M^2)
void GetCovarianceReal(ArrMatXd& , int ,int ,MatrixXf& );

/// complex version of eigenvalue solver 
void CovarianceEigenSolver(int ,int , SparseMatrix<std::complex<double> >& ,string ,
                    fftw_plan& ,fftw_complex *,fftw_complex *,ArrMatXd& ,VectorXd&  );
/// computes spatial covariance (M^2 x M^2) of Vorticity at given time
/// using sparse triplets and matrices
void GetCovarianceEigenfunc_InFreq(ArrMatXd& , int , int ,int , string ,ArrMatXd& ,VectorXd& , VectorXi& , int ,
                     fftw_plan&  , fftw_plan& , fftw_complex *,fftw_complex *,fftw_complex *);
/// Computes Covariance in Frequency domain, writes into Triplets (sparse indices and values)
void GetCovTriplet_InFreq(ArrMatXd& , int , int , VectorXi& , int , tripletlist& , fftw_plan& ,  fftw_complex * , fftw_complex *);

/// Compute Karhunen Loeve random modes Eta
/// projection onto eigenfunction space
void GetKLModes(ArrMatXd& ,VectorXd& , ArrMatXd& , int , int , int , MatrixXd& );

// -------- END Eigenvalue problem, dimensionality reduction Karhunen-Loeve-----------



/// computes orthogonal polynomials in a matrix using Gram Matrix
/// GramMatrix(i,j) =  E[(xi eta).^{mindexue(i+j,:)}]
void GetOrthonormalPolys(int , VectorXd& ,VectorXd& ,Array2D<int>& ,Array2D<int>& , Array1D<int>& , MatrixXd& );
/// given moment information and orthogonal polynomials
/// thif func calculates triple products E[T_k T_l T_m] in combined basis Xi and Eta
void GetTripleProductSparse(int ,VectorXd& ,VectorXd& , MatrixXd& ,Array2D<int>& ,Array2D<int>& ,
                    Array2D<int>& ,Array2D<int>& , Array1D<int>& ,Vector1Dusint& , Vector1D& ,Vector2Dint& ,VectorXd& );

/// computes PCE initial conditions for the next iteration
/// uses Karhunen Loeve decomposition of VorticityNew
void GetInitPCE(ArrMatXd& , int , int ,int ,int ,VectorXd& ,ArrMatXd& ,MatrixXd& ,VectorXd& ,VectorXd& ,
                    Array2D<int>& , Array2D<int>& ,Array1D<int>& , ArrMatXd& );

/// calculates L2 norm of abs value^2 of FFT2(scaled 1/sqrt(M^2)=1/M) of vorticity
/// E[|w|^2] = mean^2 + variance
void SetVorticityEnergy_InFreq(int ,int ,int , ArrMatXd& ,fftw_plan& ,fftw_complex *,fftw_complex *, ArrMatXd& ,ArrMatXd& );


/// Stochastic Navier stokes with Deterministic Viscosity  (vorticity formulation)
/// Algorithm DgPC see https://arxiv.org/abs/1605.04604
/// Moments obtained by MonteCarlo and fix time restarts
/// basis in xi and eta is combined in one multiindex
void MainLoopSNS_DetVis(int ,  int  , int, int, int , int , int , int , int ,
                        int , int , double , double , MatrixXd& , double ,
                        MatrixXcd& , MatrixXcd& , VectorXd& ,VectorXd& ,
                        MatrixXd&  , MatrixXi& ,    MatrixXi& ,
                        fftw_plan& , fftw_plan& ,fftw_complex *,fftw_complex *,fftw_complex *);

#endif
