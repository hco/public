#include "snsvor_aux.h"
#include "snsvor_sampling.h"
#include "snsvor_datafiles.h"


void init_arrmat(ArrMatXd& u, int npc1, int npc2)
{
    for(int i=0; i<npc1; ++i)
        for(int j=0; j<npc2; ++j)
            u(i,j).setZero();
}

void init_arrmat(ArrMatXcd& u, int npc1, int npc2)
{
    for(int i=0; i<npc1; ++i)
        for(int j=0; j<npc2; ++j)
            u(i,j).setZero();
}

void copy_arrmat(ArrMatXd& uin,  int npc1, int npc2, ArrMatXd& uout)
{   //given uin return uout
    init_arrmat(uout,npc1,npc2);
    for(int i=0; i<npc1; ++i)
        for(int j=0; j<npc2; ++j)
            uout(i,j) = uin(i,j);
}


void FFT1dFreqVector(int M, VectorXd& k){
    assert(M%2==0); //must be even

    for(int i=0; i<M/2.0;++i){
        k(i)= 2*M_PI*i; //
        k(M/2+i) = -2*M_PI*(M/2.0-i); 
    }

}

void eigenxd_to_fftw(MatrixXd& in, fftw_complex *out,int nx, int ny)
{
    for(int j=0; j<ny; ++j){
        for(int i=0; i<nx; ++i){
        out[i+j*nx][0] = in(j,i); 
        }
    }
}

void eigenxcd_to_fftw(MatrixXcd& in, fftw_complex *out,int nx, int ny)
{
    for(int j=0; j<ny; ++j){
        for(int i=0; i<nx; ++i){
        out[i+j*nx][0] = in(j,i).real(); // double
        out[i+j*nx][1] = in(j,i).imag();
        }
    }

}

void fftw_to_eigenxcd(fftw_complex *in, MatrixXcd& out,int nx, int ny)
{
    for(int j=0; j<ny; ++j){
        for(int i=0; i<nx; ++i){
          out(j,i).real()=in[i+j*nx][0];
          out(j,i).imag()=in[i+j*nx][1];
        }
    }

}


void fftinv_to_eigenxd(fftw_complex* in, MatrixXd& out,int nx, int ny)
{
    for(int j=0; j<ny; ++j){
        for(int i=0; i<nx; ++i){
          out(j,i)=in[i+j*nx][0]/(double)(nx*ny); ///scaled
        }
    }

}

void fft2_eigenxdc(MatrixXd& ineig, int M,fftw_plan& fft_fwd,
                  fftw_complex* in,fftw_complex* out,
                    MatrixXcd& out_eig)
{
    out_eig.setZero();
    eigenxd_to_fftw(ineig,in,M,M); // ineigen copies to in
    fftw_execute(fft_fwd); //takes transform of in write to out
    fftw_to_eigenxcd(out, out_eig, M, M); // out to out_eigen

}

void ifft2_eigenxcmatd(MatrixXcd& ineig,int M,fftw_plan& fft_inv,
                fftw_complex* out, fftw_complex* inv,MatrixXd& u)
{
    eigenxcd_to_fftw(ineig,out,M,M);
    fftw_execute(fft_inv); // ifft2 out to inv
    fftinv_to_eigenxd(inv, u, M, M);

}

void fftinv_to_arrmatxd(fftw_complex* in, ArrMatXd& out,int indx, int indy, int nx, int ny)
{
    for(int j=0; j<ny; ++j){
        for(int i=0; i<nx; ++i){
          out(indx,indy)(j,i)=in[i+j*nx][0]/(double)(nx*ny); ///scaled
        } ///writes the real part
    }

}

void ifft2_eigenxc_arrmatd(MatrixXcd& ineig, int M, fftw_plan& fft_inv,
                          fftw_complex* out, fftw_complex* inv,ArrMatXd& u,int indx,int indy)
{
    eigenxcd_to_fftw(ineig,out,M,M);
    fftw_execute(fft_inv); // ifft2 out to inv
    fftinv_to_arrmatxd(inv,u,indx,indy, M,M); //write u(ind) = inv
}




void SetSparseIndex(const int K, const int N, const int D,
                    const int K0, const int N0, MatrixXi& rvecxi, MatrixXi& rvecxieta)
{
        // number of Gaussian variables
        switch (K){

            case 4:
                  switch (N){ //degree of polynomials
                        case 2:
                              switch(D){
                                case 4:
                                      rvecxieta<<0,0,0,0,0,0,0,0,
                                                1,1,1,1,1,1,1,1,
                                                2,0,2,0,2,2,0,0;

                                    break;

                                case 6:
                                      rvecxieta<<0,0,0,0,0,0,0,0,0,0,
                                                 1,1,1,1,1,1,1,1,1,1,
                                                 2,0,2,0,2,2,2,1,0,0;

                                    break;

                                case 8:
                                      rvecxieta<<0,0,0,0,0,0,0,0,0,0,0,0,
                                                1,1,1,1,1,1,1,1,1,1,1,1,
                                                2,0,2,0,2,2,2,2,2,0,0,0;

                                    break;

                                case 10:
                                          rvecxieta<<0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                    1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                                                    2,0,2,0,2,2,2,2,2,2,0,0,0,0;
                                    break;
                              }

                        break;

                  }
                  break;

            case 8:
                   switch (N){
                        case 2:

                              switch(D){
                                case 4:
                                      rvecxieta<<0,0,0,0,0,0,0,0,0,0,0,0,
                                                1,1,1,1,1,1,1,1,1,1,1,1,
                                                2,0,2,0,2,0,2,0,2,2,0,0;

                                    break;

                                case 6:
                                      rvecxieta<<0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                 1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                                                 2,0,2,0,2,0,2,0,2,2,2,0,0,0;

                                    break;

                                case 8:
                                      rvecxieta<<0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
                                                 2,0,2,0,2,0,2,0,2,2,2,2,0,0,0,0;
                                    break;
                              }

                            break;
                   }

                break;
        }

        // for rvecxi first iteration
        switch(K0){

            case 4:
                 switch(N0){

                    case 2:
                          rvecxi<<0,0,0,0,
                                 1,1,1,1,
                                 2,2,2,2;
                        break;

                    case 3:
                           rvecxi<<0,0,0,0,
                                    1,1,1,1,
                                    2,2,2,2,
                                    3,3,3,3;
                        break;
                 }
                 
                break;

            case 6:

                if(N0==2){
                     rvecxi<<0,0,0,0,0,0,
                             1,1,1,1,1,1,
                             2,1,0,2,1,0;
                }

                break;

            case 8:

                if(N0==2){
                        rvecxi<<0,0,0,0,0,0,0,0,
                                1,1,1,1,1,1,1,1,
                                2,1,2,1,2,1,2,1;
                }

                break;

        }
}


void eigentouqcopy(MatrixXi& rvec,Array2D<int>& rvecint)
{
    int nr=rvec.rows(),nc=rvec.cols();
    rvecint.Resize(nr,nc);
    for(int i=0; i<nr;++i)
        for(int j=0; j< nc;++j)
            rvecint(i,j) = rvec(i,j);

}

void fill_rmat(Vector3Dint& rmat, Array2D<int>& rvec,int N)
{

    rmat.push_back(rvec);
    Array2D<int> temp;

    for(int i=1; i<3*N; ++i){
        temp=radd(rmat[i-1],rvec);
        rmat.push_back(temp);
    }

}

void IntegerPartition(int n, Vector2Dxi& i_part)
{

    int s=2; // always partition into 2
    VectorXi a(n);
    a.setZero();
    int k=1,l;
    int y=n-1,x;

    while (k != 0){
        x = a(k-1) + 1;
        k = k-1;
        while (2*x <= y){
            a(k) = x;
            y = y - x;
            k = k + 1;
        }
        l = k + 1;
        while (x <= y){
            a(k) = x;
            a(l) = y;
            if (k+2<=s){
                i_part.push_back(a.head(k+2));
            }
            x = x + 1;
            y = y - 1;
        }
        a(k) = x + y;
        y = x + y - 1;
        if (k+1<=s){
             i_part.push_back(a.head(k+1));
        }
    }
     i_part.pop_back(); //remove last

}

Array2D<int> radd(Array2D<int>& r1, Array2D<int>& r2)
{

    int N1,N2,K1,K2;

    N1= r1.XSize()-1;
    K1= r1.YSize();
    N2= r2.XSize()-1;
    K2= r2.YSize();

    assert(K1==K2 && N1 >= N2);
    int K=K1;
    int N=N1+N2;
    Array2D<int> r(N+1,K,0);


    for(int i=0; i<K; ++i){
        r(0,i) = r1(0,i) + r2(0,i);
        r(1,i) = r1(1,i) + r2(0,i);
    }
    Vector2Dxi tmp;
    int nd,i2,i1;

    for(int i=2;i<=N;++i){
        IntegerPartition(i,tmp);
        nd=tmp.size();

        for(int j=0; j<nd;++j){
            i2=tmp[j](0);
            i1=tmp[j](1);
            if(i1<=N1 && i2<=N2){
                for(int k=0; k<K; ++k)
                    r(i,k) = r1(i1,k) + r2(i2,k);
              //break;
            }
        }
    }

    return r;


}

void multi_ind_mat_sp(Array2D<int>& mindex,int K, Array2D<int>& rvec, Array2D<int>& mindexsp, Array1D<int>& indsp)
{

      int npc= mindex.XSize();

      Vector2Dxi tmpsp(0);
      VectorXi indtmp;
      indtmp.resize(npc+1);
      indtmp.setOnes();
      indtmp*=-1; //-1 all sparse indices

      Array1D<int> rx(K),tmp;
      int xord,nnzrx;
      VectorXi x(K);

     int n=0;
     for(int i=0; i< npc; ++i){
        for(int j=0; j<K; ++j)
            x(j) = mindex(i,j);
         xord=x.sum();
         for(int j=0; j<K; ++j)
            rx(j) = rvec(xord,j) - x(j);
         find_ind(rx,0,"ge",tmp);
         nnzrx = tmp.XSize();
         if(nnzrx ==K){
            tmpsp.push_back(x);
            indtmp(i)=n;
            n++;
         }

     }
     // dump into mindexsp
     mindexsp.Resize(tmpsp.size(),K);
     for(unsigned int i=0;i< tmpsp.size(); ++i){
        for(int j=0; j<K; ++j)
            mindexsp(i,j) = tmpsp[i](j);
     }
     indsp.Resize(indtmp.size());
     for(int i=0;i< indtmp.size(); ++i)
        indsp(i)= indtmp(i);

}


void SolveLaplace(int M, MatrixXd& k2, MatrixXcd& rhs,MatrixXcd& f_psi)
{
    f_psi.setZero();

    for(int i=0; i<M; ++i)
        for(int j=0; j<M; ++j)
            if(i+j>0) // ignore (0,0) freq
                f_psi(i,j) = rhs(i,j)/k2(i,j);
}


void  gaussian_mom(double mu,double sigma, int Sz, VectorXd& mom_vec)
{   // mu= mean, sigma=std deviation
    // write to mom_vec
    mom_vec.setZero();
    mom_vec(0)=1;  //# to adjust the index
    mom_vec(1) = mu;
    mom_vec(2) = pow(mu,2)+ pow(sigma,2);

    for(int n=3; n<Sz+1; n++)
        for(int p=0; p<floor((double)n/2.0)+1; p++){
            mom_vec(n) +=  nChoosek(n,2*p) * double_factorial(2*p-1) * pow(sigma,(2*p)) * pow(mu,(n-2*p));}
}


void  get_multimeans_xi(Array2D<int>& mindexue3,int K,VectorXd& mom_vecxi1d,VectorXd& meansxi)
{
     //return multi-index moments in meansxi
     
     int npcue3= mindexue3.XSize();

     Array1D<int> mnd(K);
     double sm;

     for(int i=0; i< npcue3; ++i){
        mnd=getRowtop(mindexue3, i, K); // this is for xi
        sm=1;
        for(int k=0; k<K; ++k)
            sm*= mom_vecxi1d(mnd(k));

        meansxi(i) = sm;

     }
}

void trip_prod_wick_sp(Vector1D& trip_val, Array2D<int>& mindex, Vector2Dint& ijkvec)
{
    // return sparse indices in ijkvec and values trip_val
    
    int npc= mindex.XSize();
    int K= mindex.YSize();
    double sm;
    Array1D<int> temp(3,0);

    double tol=1e-5;

    for(int i=0; i<npc; ++i){
        for(int j=0; j<npc; ++j){
            for(int k=0; k<npc; ++k){ 

                sm=1.0;
                for(int n=0; n<K; ++n)
                    sm*= hermite_trip_int(mindex(i,n), mindex(j,n),mindex(k,n))/
                         sqrt(exp(lgamma(mindex(i,n)+1)+lgamma(mindex(j,n)+1)
                                +lgamma(mindex(k,n)+1))); // 1d triple products

                if(abs(sm)>tol){ //ignore below threshold
                    trip_val.push_back(sm);
                    temp(0) =i; temp(1)=j; temp(2)=k;
                    ijkvec.push_back(temp);
                }
                 temp.SetValue(0);
            }
        }
    }
}


void trip_prod_cmbnd_preind(int npcue, Array2D<int>& mindexue,Array2D<int>& w_matue3,
                     Array1D<int>& indue3sp, Vector1Dusint& trip_usind)
{   
    /// return in short int vector trip_usind
    
    cout<<"Triple products pre-computation of indices..."<<endl;
    int ind;
    int npcue3f= indue3sp.XSize()-1;
    //E[T_k T_l T_m]
    for(int k=0;k<npcue;++k){
        for(int m=0; m<npcue; ++m){
            for(int l=0; l<npcue;++l){
                 for(int kl=0; kl<k+1;++kl){
                    for(int ml=0; ml<m+1;++ml){
                        for(int ll=0; ll<l+1;++ll){
                                ind= indue3sp(ret_indexsp(getRowmod(mindexue,kl)+getRowmod(mindexue,ml)+getRowmod(mindexue,ll),w_matue3,npcue3f));
                                if(ind>=0)
                                    trip_usind.push_back((unsigned short)ind);
                        }
                    }
                 }
            }
        }
    }

    cout<<"Triple products pre-computation of indices DONE"<<endl;
}



void exp_mat(int M, double vis, MatrixXd& k2,double t, MatrixXd& Ex1)
{
    for(int i=0; i<M; ++i)
        for(int j=0; j<M; ++j)
            Ex1(i,j) = exp(vis*k2(i,j)*t); // exp(vis*k^2*t(i))
            
}

MatrixXd sum2_arrmat(ArrMatXd& w,int nx,int ny)
{
    /// sum(w.^2)
    int M = w(0).rows();
    MatrixXd temp(M,M); temp.setZero();

    for(int i=0; i< nx; ++i ){
        for(int j=0; j<ny; ++j){
            temp  += w(i,j).cwiseProduct(w(i,j));

        }
    }
    return temp;
}

void ProductTwoPCEs(int M, int npc, ArrMatXd& u,ArrMatXd& v, ArrMatXd& uv,
                      Vector1D& TripProd, Vector2Dint& TripInd, int TripProdIndSz)
{
    // given u & v, return uv
    
    int i,j,k,l;

    init_arrmat(uv,npc,1);

    for(l=0; l<TripProdIndSz; ++l){
        i = TripInd[l](0); 
        j = TripInd[l](1); 
        k = TripInd[l](2);
        uv(i) += v(j).cwiseProduct(u(k))*TripProd[l];
    }

}

void FFT2_AllPCEs(int npc, int M, ArrMatXd& In1, ArrMatXd& In2,
              MatrixXd& tempd, MatrixXcd& tempc, fftw_plan& fft_fwd, fftw_complex *in,fftw_complex *out,
              ArrMatXcd& Out1, ArrMatXcd& Out2)
{      /// In1 & In2 FFT --> writes to Out1 Out2
       for(int i=0; i<npc; ++i){
                tempd= In1(i);
                fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
                Out1(i) = tempc;
                tempd= In2(i);
                fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
                Out2(i) = tempc;
        }
}


void IFFT2_ComputeVelocity(int npc, int M,MatrixXd& Exwinv, MatrixXd& tempd, MatrixXcd& tempc,
                ArrMatXcd& F_Vor, ArrMatXd& Vor,MatrixXcd& f_psi,  MatrixXcd& kixmat,
                MatrixXcd& kiymat,MatrixXd& k2,fftw_plan& fft_fwd,fftw_plan& fft_inv,
                fftw_complex *in,fftw_complex *out,fftw_complex *inv,
                ArrMatXd& u, ArrMatXd& v){
      // calculate velocity components (u,v)
      for(int i=0; i<npc; ++i){

        tempc= Exwinv.cwiseProduct(F_Vor(i));
        ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,Vor,i,0);

        tempd= Vor(i);
        fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
        SolveLaplace(M,k2,tempc,f_psi);
        tempc= kiymat.cwiseProduct(f_psi);  // kiy*f_psi (d_y in Fourier domain)
        ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,u,i,0);
        tempc= -kixmat.cwiseProduct(f_psi); // kix*f_psi (d_x in Fourier domain)
        ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,v,i,0);

    }
}

void EvalRhsUsingExp(double t,double Ti, double Tf,MatrixXd& ExpMat, int K,int K1, int npc,
                MatrixXcd& kximat,MatrixXcd& kyimat,
                ArrMatXcd& f_uVor,ArrMatXcd& f_vVor,
                MatrixXcd& f_dsigma1,MatrixXcd& f_dsigma2,
                ArrMatXcd& rhsw)
{
     //uses exponential integrator scheme for the stiff term 
     init_arrmat(rhsw,npc,1); // right hand side of SNS will be returned

     for(int a=0; a<npc; ++a){

            if(a>=1 && a<K1){ // -Exp.*(kix*Fu + kiy*Fv +\dsigma_1*dW_1)  
                rhsw(a) = - ExpMat.cwiseProduct(f_uVor(a).cwiseProduct(kximat) + f_vVor(a).cwiseProduct(kyimat) +
                                            cal_mif(a,t,Ti,Tf)*f_dsigma1);
            }else if(a>=K1 && a<=K){ //-Exp.*(kix*Fu + kiy*Fv -\dsigma_2*dW_2)  
                rhsw(a) = - ExpMat.cwiseProduct(f_uVor(a).cwiseProduct(kximat) + f_vVor(a).cwiseProduct(kyimat) -
                                                        cal_mif(a-K1+1,t,Ti,Tf)*f_dsigma2);
            }else{ // no random forcing
                //-Exp.*(kix*Fu + kiy*Fiv) 
                rhsw(a) = - ExpMat.cwiseProduct(f_uVor(a).cwiseProduct(kximat) + f_vVor(a).cwiseProduct(kyimat));
            }
    }
}


void one_step_RK4(double Ti,double Tf, double t1, double dt, double Vis,int K,int K1, int M, int npc,
                 ArrMatXcd& F_Vor0, ArrMatXcd& rhs0w, MatrixXcd& kixmat, MatrixXcd& kiymat,MatrixXd& k2,
                 MatrixXcd& f_dsigma1,MatrixXcd& f_dsigma2,
                 Vector1D& TripProd, Vector2Dint& TripProdInd, int TripProdIndSz,
                 fftw_plan& fft_fwd,fftw_plan& fft_inv,
                 fftw_complex *in,fftw_complex *out,fftw_complex *inv,
                 ArrMatXcd& F_Vor1)
{       // one step RK4 for initialization for Predictor Corrector 4 (PC4) scheme
        // between [Ti,Tf] writes F_vor1 at Tf
        
        ArrMatXcd k1w(npc,1), k2w(npc,1), k3w(npc,1), k4w(npc,1);
        MatrixXd  Exw(M,M), Exwinv(M,M), tempd(M,M);

        ArrMatXcd f_uVor2(npc,1),f_vVor2(npc,1);
        MatrixXcd tempc(M,M),f_psi(M,M);

        ArrMatXd uVor2(npc,1),vVor2(npc,1); // up to npcu
        ArrMatXd u1(npc,1), v1(npc,1), Vor1(npc,1);
        init_arrmat(u1,npc,1);
        init_arrmat(v1,npc,1);

        k1w= rhs0w;

        for(int i=0; i<npc; ++i)
            F_Vor1(i) = F_Vor0(i) + (dt/2)*k1w(i); // dt/2*k1

        exp_mat(M,Vis,k2,(t1+dt/2-Ti),Exw);
        exp_mat(M,Vis,k2,-(t1+dt/2-Ti),Exwinv);

        IFFT2_ComputeVelocity( npc,  M ,Exwinv, tempd,tempc, F_Vor1, Vor1,
            f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripProdInd,TripProdIndSz);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2,TripProd, TripProdInd,TripProdIndSz);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd,in,out, f_uVor2,  f_vVor2);

        EvalRhsUsingExp(t1+dt/2,Ti,Tf,Exw,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,k2w);

        for(int i=0; i<npc; ++i)
            F_Vor1(i) = F_Vor0(i) + (dt/2)*k2w(i); // dt/2*k2

        IFFT2_ComputeVelocity( npc,  M ,Exwinv, tempd,tempc, F_Vor1, Vor1,
            f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripProdInd,TripProdIndSz);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2,TripProd, TripProdInd,TripProdIndSz);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd,in,out, f_uVor2,  f_vVor2);

        EvalRhsUsingExp(t1+dt/2,Ti,Tf,Exw,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,k3w);

        for(int i=0; i<npc; ++i)
            F_Vor1(i) = F_Vor0(i) + dt*k3w(i); // dt*k3

        exp_mat(M,Vis,k2,-(t1+dt-Ti),Exwinv);

        IFFT2_ComputeVelocity( npc,  M ,Exwinv, tempd,tempc, F_Vor1, Vor1,
            f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripProdInd,TripProdIndSz);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2,TripProd, TripProdInd,TripProdIndSz);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd,in,out, f_uVor2,  f_vVor2);

        exp_mat(M,Vis,k2,(t1+dt-Ti),Exw);

        EvalRhsUsingExp(t1+dt,Ti,Tf,Exw,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,k4w);

         for(int i=0; i<npc; ++i)
            F_Vor1(i) =  F_Vor0(i) + (dt/6.0)*(k1w(i)+2*k2w(i)+2*k3w(i)+k4w(i)); //RK4
}


void  ODEint_SNS_DetVis_PC4(double Ti,double Tf,int Res_cnt,int K, int M,int npc,
                int nt, double Vis, MatrixXcd& kixmat,  MatrixXcd& kiymat,MatrixXd& k2,
                MatrixXcd& f_dsigma1,MatrixXcd& f_dsigma2,
                ArrMatXd& Vor1, Vector1D& TripProd, Vector2Dint& TripIndices, int TripIndSize, ArrMatXd& MeanVor,ArrMatXd& VarVor,
                fftw_plan& fft_fwd, fftw_plan& fft_inv,fftw_complex *in,fftw_complex *out,fftw_complex *inv,
                ArrMatXd& VorEnd)
{   
    // writes into MeanVor and VarVor and returns final vorticity VorEnd
    // Predictor-Corrector 4th order 

    double dt= (Tf-Ti)/(double)nt; // time step

    ArrMatXd u1(npc,1), v1(npc,1); // velocity field (u,v)
    init_arrmat(u1,npc,1);
    init_arrmat(v1,npc,1);

    ArrMatXcd F_Vor0(npc,1), F_Vor1(npc,1); // FFT's for vorticity
    init_arrmat(F_Vor0,npc,1); init_arrmat(F_Vor1,npc,1);

    MatrixXd tempd(M,M);
    MatrixXcd f_psi(M,M),tempc(M,M);

    ArrMatXcd rhs1w(npc,1),rhs0w(npc,1), rhs2w(npc,1),rhs3w(npc,1); //right sides for RK4 nd PC4
    init_arrmat(rhs0w,npc,1);
    init_arrmat(rhs1w,npc,1);
    init_arrmat(rhs2w,npc,1);
    init_arrmat(rhs3w,npc,1);


    double t1=Ti;
    MatrixXd ExpVisK2(M,M), ExpInvVisK2(M,M); // exp(vis*k^2*t)
    ArrMatXd ExpVisK2_mat(nt+1,1) ,ExpInvVisK2_mat(nt+1,1);
    init_arrmat(ExpVisK2_mat,nt+1,1); init_arrmat(ExpInvVisK2_mat,nt+1,1);

    for(int it=0; it<nt+1; ++it){
        exp_mat(M,Vis,k2,(t1-Ti),ExpVisK2);
        exp_mat(M,Vis,k2,-(t1-Ti),ExpInvVisK2);
        ExpVisK2_mat(it) = ExpVisK2;
        ExpInvVisK2_mat(it) = ExpInvVisK2;

        t1+=dt;
    }

    ///--------------------------------initials
    for(int i=0; i<npc;++i){
        tempd= Vor1(i);
        fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
        F_Vor0(i) = tempc;
        SolveLaplace(M,k2,tempc,f_psi);
        tempc= kiymat.cwiseProduct(f_psi);
        ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,u1,i,0); // take inverse write to u1(i)
        tempc= -kixmat.cwiseProduct(f_psi);
        ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,v1,i,0); //v1(i)
    }

    ArrMatXd uVor2(npc,1),vVor2(npc,1); // product of vorticity and velocity components
    ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripIndices,TripIndSize);
    ProductTwoPCEs(M,npc,v1,Vor1, vVor2, TripProd, TripIndices,TripIndSize);

    ArrMatXcd f_uVor2(npc,1),f_vVor2(npc,1);

    FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd, in,out, f_uVor2,  f_vVor2);
    ///---------------------------END initials
    
    
    cout<<"...... integrating between "<< Ti<<" and " <<Tf<<endl;
    int K1= K/2+1;
    t1=Ti;
    /// 3 step RK4 to init PC4
    for(int it=0; it<3; ++it){

        ExpVisK2=ExpVisK2_mat(it);

        EvalRhsUsingExp(t1,Ti,Tf,ExpVisK2,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,rhs3w);

        one_step_RK4(Ti,Tf,t1, dt, Vis, K,K1, M, npc,
                  F_Vor0, rhs3w,kixmat,kiymat, k2, f_dsigma1,  f_dsigma2,
                  TripProd,  TripIndices, TripIndSize,fft_fwd,fft_inv,in,out,inv, F_Vor1);

        ExpInvVisK2= ExpInvVisK2_mat(it+1);

        IFFT2_ComputeVelocity( npc,  M, ExpInvVisK2, tempd,tempc, F_Vor1, Vor1,
                  f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripIndices,TripIndSize);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2, TripProd, TripIndices,TripIndSize);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd, in,out, f_uVor2,  f_vVor2);

        for(int i=0; i<npc;++i)
            F_Vor0(i) = F_Vor1(i);

        if(it==0){
            rhs0w= rhs3w;
        }else if(it==1){
            rhs1w= rhs3w;
        }else if(it==2){
            rhs2w= rhs3w;
        }

        t1+=dt;
    }
    init_arrmat(rhs3w,npc,1);


    // remaining (t>= t_3) ode integration done by PC4
    // weights prediction [-9/24  37/24 -59/24 55/24];
    // weights correction [ 1/24 -5/24   19/24 9/24 ];
    for(int it=3; it<nt; ++it){

        ExpVisK2= ExpVisK2_mat(it);
        EvalRhsUsingExp(t1,Ti,Tf,ExpVisK2,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,rhs3w);

        for(int i=0; i<npc; ++i)
                F_Vor1(i) = F_Vor0(i) + (dt/24.0)*( 55*rhs3w(i)-59*rhs2w(i)+37*rhs1w(i)-9*rhs0w(i) );

        ExpInvVisK2= ExpInvVisK2_mat(it+1);
        IFFT2_ComputeVelocity( npc,  M, ExpInvVisK2, tempd,tempc, F_Vor1, Vor1,
                  f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripIndices,TripIndSize);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2, TripProd, TripIndices,TripIndSize);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd, in,out, f_uVor2,  f_vVor2);

        ExpVisK2= ExpVisK2_mat(it+1);
        EvalRhsUsingExp(t1+dt,Ti,Tf,ExpVisK2,K,K1,npc,kixmat,kiymat, f_uVor2,f_vVor2,f_dsigma1,f_dsigma2,rhs0w);

        for(int i=0; i<npc; ++i)
                F_Vor1(i) = F_Vor0(i) + (dt/24.0)*( 9*rhs0w(i)+19*rhs3w(i) -5*rhs2w(i) + rhs1w(i) );

        IFFT2_ComputeVelocity( npc,  M, ExpInvVisK2, tempd,tempc, F_Vor1, Vor1,
                  f_psi,kixmat,kiymat, k2,fft_fwd, fft_inv,in, out,inv, u1, v1);

        ProductTwoPCEs(M,npc,u1,Vor1, uVor2, TripProd, TripIndices,TripIndSize);
        ProductTwoPCEs(M,npc,v1,Vor1, vVor2, TripProd, TripIndices,TripIndSize);

        FFT2_AllPCEs( npc, M,  uVor2,  vVor2, tempd,  tempc,  fft_fwd, in,out, f_uVor2,  f_vVor2);

        for(int i=0; i<npc;++i){
            F_Vor0(i) = F_Vor1(i);

            rhs0w(i)= rhs1w(i);
            rhs1w(i)= rhs2w(i);
            rhs2w(i)= rhs3w(i);
        }

        t1+=dt;

    }

    MeanVor(Res_cnt+1) = Vor1(0);
    VarVor(Res_cnt+1) = sum2_arrmat(Vor1,npc,1) - MeanVor(Res_cnt+1).cwiseProduct(MeanVor(Res_cnt+1)); // sum_{i}^npcu w1(i)^p - mean^2

    VorEnd= Vor1;
}


void SetHigherMoments(ArrMatXd& u1 ,int M,int cnt,int npc,Vector1D& TripProd,Vector2Dint& TripInd,int TripProdSz,
                    ArrMatXd& u3mat, ArrMatXd& u4mat)
{   // given PCE u1 
    // writes to u3mat, u4mat
    ArrMatXd u2(npc,1), u3(npc,1),u4(npc,1);

    ProductTwoPCEs(M,npc,u1,u1,u2,TripProd,TripInd,TripProdSz);
    ProductTwoPCEs(M,npc,u2,u1,u3,TripProd,TripInd,TripProdSz);
    ProductTwoPCEs(M,npc,u2,u2,u4,TripProd,TripInd,TripProdSz);

    for(int  i=0; i<M; ++i){
            for(int j=0; j<M; ++j){
               u3mat(cnt)(i,j) = u3(0)(i,j) -3*u2(0)(i,j)*u1(0)(i,j) +3*pow(u1(0)(i,j),3)-pow(u1(0)(i,j),3);
               u4mat(cnt)(i,j) = u4(0)(i,j) -4*u3(0)(i,j)*u1(0)(i,j) +6*u2(0)(i,j)*pow(u1(0)(i,j),2) -3*pow(u1(0)(i,j),4);
            }
    }

}



void NormalizeEigenfunc(ArrMatXd& EigFunc,int D, int M)
{
    double sm;
    for(int j=0; j<D; ++j){
        sm=0;
        for(int k=0; k< EigFunc.cols();++k)
            sm += ( EigFunc(j,k).cwiseProduct(EigFunc(j,k)) ).sum();

        EigFunc(j)/=sqrt( (1.0/(double)pow(M,2))*sm );
    }
}


//double Covariance
void CovarianceEigenSolver(int D,int M, SparseMatrix<float>& CovarianceSparse, string EigsStr,
                  ArrMatXd& EigFunc,VectorXd& Eigs )
 {      // writes into EigFunc and Eigs 
        
        long M2=pow(M,2);

        ArpackGeneralizedSelfAdjointEigenSolver<SparseMatrix<float> > ev(CovarianceSparse,D,EigsStr);

        for(int i=0; i<D;++i){
            for(int a=0; a<M;++a){
                for(int b=0; b<M; ++b){
                    for(int k=0; k<EigFunc.cols(); ++k)
                        EigFunc(i,k)(a,b) = (double) ev.eigenvectors().col((D-1)-i)( (long)M2*k+(b+a*M) );
                    }
                }
            Eigs(i) = (double) fabs(ev.eigenvalues()((D-1)-i)); // fabs if e.g. -1e-10
        }

        NormalizeEigenfunc(EigFunc,D,M);

}
 
 
void GetCovarianceEigenfunc(ArrMatXd& Vorend, int npc, int M,int D,string EigsStr,ArrMatXd& EigfuncVor,VectorXd& Eigs)
{       //float implementation 

        init_arrmat(EigfuncVor,EigfuncVor.rows(),EigfuncVor.cols()); //  returned

        MatrixXf Covariance;
        SparseMatrix<float> CovarianceSparse;

        boost::progress_timer t;

        int randomDim = EigfuncVor.cols();

        Covariance.resize((long)randomDim*pow(M,2),(long)randomDim*pow(M,2)); // random dimension only vorticity

        GetCovarianceReal(Vorend, npc,M, Covariance);

        CovarianceSparse=Covariance.sparseView(0.1,0.000000000001); //removes < 1e-13
       
        Covariance.resize(0,0);
        //real eigenvalue solver
        CovarianceEigenSolver(D, M, CovarianceSparse,  EigsStr, EigfuncVor, Eigs );

}


void GetCovarianceReal(ArrMatXd& Vorend, int npc,int M,MatrixXf& Cov)
{

    Cov.setZero();
    int M2= (int) pow(M,2);

    MatrixXd Vorlinear(npc, M2); 
    for(int a=0;a<npc;++a)
        for(int i=0; i<M; ++i)
            for(int j=0; j<M; ++j)
                Vorlinear(a,j+M*i) = Vorend(a)(i,j);

    // computation of left upper block
    // the formula given by PCE's
    for(int i=0; i<M2; ++i){
        for(int k=i; k<M2; ++k){
            for(int a=1; a<npc;++a)
                Cov(i,k) +=(float) (1.0/(float)M2)*Vorlinear(a,i)*Vorlinear(a,k);
            Cov(k,i) = Cov(i,k); //symmetry
        }
    }

}

// complex Covariance
void CovarianceEigenSolver(int D,int M, SparseMatrix<std::complex<double> >& CovarianceSparse,
                string EigsStr,fftw_plan& fft_inv,fftw_complex *out,fftw_complex *inv,
                ArrMatXd& EigFunc,VectorXd& Eigs )
 {      
        //return EigFunc and Eigs
     
        double M2=pow((double)M,2);
        //Complex eigenvalue solver only works for complex double for now
        ArpackGeneralizedEigenSolver<SparseMatrix<std::complex<double> > >
                                                                    ev(CovarianceSparse,D,EigsStr);
        int RandomDim = EigFunc.cols();
        ArrMatXcd  tempc_arrmat(D,RandomDim);
        // get eigenvalues and complex eigenfunctions
        for(int i=0; i<D;++i){
             Eigs(i) = fabs(ev.eigenvalues()(i).real()/M2); //divide M^2
             for(int a=0; a<M;++a){
                    for(int b=0; b<M; ++b){
                        for(int k=0; k<RandomDim; ++k){
                            tempc_arrmat(i,k)(a,b)  = ev.eigenvectors().col(i)((long)M2*k + (b+a*M));
                        }
                    }
             }
        }
        // inverse FFT eigenfunctions
        MatrixXcd tempc;
         for(int i=0; i<D;++i){
            for(int k=0; k<RandomDim; ++k){
                tempc = tempc_arrmat(i,k);
                ifft2_eigenxc_arrmatd(tempc,M,fft_inv,out,inv,EigFunc,i,k);
            }
        }
        
        NormalizeEigenfunc(EigFunc,D,M);
}




void GetCovarianceEigenfunc_InFreq(ArrMatXd& Vorend, int npc, int M,int D,
                     string EigsStr,ArrMatXd& EigfuncVor,VectorXd& Eigs, VectorXi& SpIndex, int SpIndexSz,
                     fftw_plan& fft_fwd , fftw_plan& fft_inv, fftw_complex *in,fftw_complex *out,fftw_complex *inv)
{       
        init_arrmat(EigfuncVor,EigfuncVor.rows(),EigfuncVor.cols()); //returned

        int M2= (int) pow(M,2);
        boost::progress_timer t;
        int randomDim = EigfuncVor.cols();

        SparseMatrix<std::complex<double> > CovarianceSparse( (long)randomDim*M2,(long) randomDim*M2 );

        tripletlist  CovarianceTriplets;

        cout<<"sparse matrix triplets"<<endl;
       // CovarianceTriplets.reserve(pow(2*SpIndexSz*M,2));  /// reserve space for cov
        GetCovTriplet_InFreq(Vorend, npc,M, SpIndex, SpIndexSz, CovarianceTriplets, fft_fwd, in, out);

        CovarianceSparse.setFromTriplets(CovarianceTriplets.begin(),CovarianceTriplets.end());
        CovarianceTriplets.clear();
        CovarianceSparse.makeCompressed();

        CovarianceEigenSolver(D, M, CovarianceSparse, EigsStr, fft_inv, out, inv, EigfuncVor, Eigs );

}

void GetCovTriplet_InFreq(ArrMatXd& VorEnd, int npc, int M, VectorXi& SpIndex, int SpIndexSz,
            tripletlist& CovarianceTrip, fftw_plan& fft_fwd,  fftw_complex *in , fftw_complex *out)
{   
    // return CovarianceTrip
    
    int ll,kk, M2= (int) pow(M,2);

     MatrixXd tempd(M,M); MatrixXcd tempc(M,M);
     ArrMatXcd f_Vorend(npc,1);
     for(int i=0; i<npc; ++i){
                tempd = VorEnd(i);
                fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
                f_Vorend(i) = tempc;
    }

    MatrixXcd  f_Vorlinear(npc,M2);
    for(int a=0;a<npc;++a)
        for(int i=0; i<M; ++i)
            for(int j=0; j<M; ++j)
                f_Vorlinear(a,j+M*i) = f_Vorend(a)(i,j);


    std::complex<double> sm;
    std::complex<double> cM2 = 1.0/(double)M2+ 0*1j;
    
    // compute sparse triplets
    for(int l=0;l < (int)SpIndexSz*M; ++l){ ///column major
        ll= SpIndex(l);
        for(int k=l; k< (int)SpIndexSz*M; ++k){
            kk = SpIndex(k);
            sm=0+0*1j;
            for(int a=1; a<npc;++a)
                sm+= cM2*f_Vorlinear(a,kk)*conj(f_Vorlinear(a,ll));

            CovarianceTrip.push_back(triplet(kk,ll,(complex<double>)sm));
            if(kk!=ll) // symmetry
                CovarianceTrip.push_back(triplet(ll,kk, (complex<double>) conj(sm)));
        }
    }
}


void GetKLModes(ArrMatXd& Vorend,VectorXd& Eigs, ArrMatXd& EigfuncVor, int npc, int M, int D, MatrixXd& Eta)
{   // Eta computed

    Eta.setZero();

    for(int j=0; j<D; ++j)
        for(int b=1; b<npc; ++b)
            Eta(b,j)= 1.0/(pow(M,2)*sqrt(Eigs(j)))* (Vorend(b).cwiseProduct(EigfuncVor(j))).sum();

}



void GetOrthonormalPolys(int npc, VectorXd& MeansXi,VectorXd& MeansEta,
                          Array2D<int>& mindexue,Array2D<int>& WeightMatue3, Array1D<int>& indue3sp,
                          MatrixXd& OrthPolyMat)
{

    MatrixXd GramMatrix(npc,npc);
    GramMatrix.setZero();

    int ind;
    int npcue3f= indue3sp.XSize()-1;

    for(int i=0; i<npc; ++i){
        for(int j=0; j<npc;++j){
             ind= indue3sp(ret_indexsp(getRowmod(mindexue,i)+getRowmod(mindexue,j),WeightMatue3,npcue3f));
             if(ind>=0)
                GramMatrix(i,j) = MeansXi(ind) * MeansEta(ind);
        }
    }
    // Gram-Schmidt through Cholesky
    MatrixXd R= GramMatrix.llt().matrixU();
    OrthPolyMat= (R.inverse()).transpose();

}


void GetTripleProductSparse(int npcue,VectorXd& MeansXi,VectorXd& MeansEta, MatrixXd& OrthPoly,
                    Array2D<int>& mindexue,Array2D<int>& w_matue,
                    Array2D<int>& w_matue2,Array2D<int>& w_matue3, Array1D<int>& indue3sp,
                    Vector1Dusint& TripIndexStored,
                    Vector1D& TripProds,Vector2Dint& TripProdIndex,VectorXd& uemean)
{


    TripProds.clear(); //to be written
    TripProdIndex.clear(); // to be written


    double tol=1e-3,sm;
    VectorXd uemeansq(npcue); 
    Array1D<int> temp(3);

    uemean.setZero();
    uemeansq.setZero();

    unsigned short ind, npcue3f= indue3sp.XSize()-1;
    for(int k=0;k<npcue;++k){
        //mean E[T_k]
        for(int kl=0; kl<k+1;++kl){
            ind=indue3sp(ret_indexsp(getRowmod(mindexue,kl),w_matue,npcue3f));
            uemean(k)+=OrthPoly(k,kl)*MeansXi(ind)*MeansEta(ind);
            //mean.^2 E[T_k^2]
            for(int km=0; km<k+1;++km){
                ind= indue3sp(ret_indexsp(getRowmod(mindexue,kl)+getRowmod(mindexue,km),w_matue2,npcue3f));
                uemeansq(k)+= OrthPoly(k,kl)*OrthPoly(k,km)*MeansXi(ind)*MeansEta(ind);
           }
        }

    }

    cout<<"Trip products computation..."<<endl;
    //compute E[T_k T_l T_m]
    unsigned long long i=0;
    for(int k=0;k<npcue;++k){
        for(int m=0; m<npcue; ++m){
            for(int l=0; l<npcue;++l){
                sm=0;
                 for(int kl=0; kl<k+1;++kl){
                    for(int ml=0; ml<m+1;++ml){
                        for(int ll=0; ll<l+1;++ll){
                              ind = TripIndexStored[i++];
                              sm += OrthPoly(k,kl)*OrthPoly(m,ml)*OrthPoly(l,ll)*MeansXi(ind)*MeansEta(ind);
                        }
                    }
                 }
                if(uemeansq(k)>0 && uemeansq(l)>0 && uemeansq(m)>0){
                    sm/=sqrt(uemeansq(k)*uemeansq(l)*uemeansq(m));

                     if(abs(sm)>tol){ /// \todo above if
                        TripProds.push_back(sm);
                        temp(0)=k; temp(1)=l; temp(2)=m;
                        TripProdIndex.push_back(temp);
                    }
                }

            }
        }
    }
    // checking orthogonality
   // cout<<"uemean"<<endl<<uemean.transpose()<<endl;
   // cout<<"uemeansqm"<<endl<<uemeansq.transpose()<<endl;

}


void GetInitPCE(ArrMatXd& PCend, int K, int M,int npcue,int D,VectorXd& Eigs,ArrMatXd& EigenFunc,
                    MatrixXd& OrthPoly,VectorXd& MeansXi,VectorXd& MeansEta,
                    Array2D<int>& mindexue, Array2D<int>& wmat3ue,
                    Array1D<int>& indue3sp, ArrMatXd& PCnew)
{

     init_arrmat(PCnew,PCnew.rows(),PCnew.cols()); //written

     int  npcue3=indue3sp.XSize()-1;
     Array1D<int> mnd(mindexue.YSize());

      double sm=0; int ind;
      for(int a=0; a<npcue;++a){
            if(a==0){
                    PCnew(0) = PCend(0); // mean
            }else{
                for(int j=0;j<D;++j){
                        mnd= getRowmod(mindexue,K+j+1); // this corresponds to Eta

                        sm=0;
                        // sm=E[ Eta T_a]
                        for(int al=0;al<=a;++al){
                            ind = indue3sp(ret_indexsp(getRowmod(mindexue,al)+mnd,wmat3ue,npcue3));
                            sm+=OrthPoly(a,al)*MeansXi(ind)*MeansEta(ind);
                        }
                    // Pcnew = sum_{j=1}^D sqrt(Eigs_j) EigenFunc_j E[Eta_j T_a]
                    PCnew(a)+= sqrt(Eigs(j))*sm*EigenFunc(j);
                }

            }

    }
}


void SetVorticityEnergy_InFreq(int M,int npc,int Res_cnt, ArrMatXd& VorEnd,
                                fftw_plan& fft_fwd,fftw_complex *in,fftw_complex *out,
                                ArrMatXd& EnergyTotal,ArrMatXd& EnergyVariance)
{
    MatrixXd tempd(M,M), kint(M,M),kinv(M,M);
    MatrixXcd tempc(M,M);
    kint.setZero();
    kinv.setZero();

    //mean
    tempd= VorEnd(0);
    fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
    tempc/=(double)pow(M,2); // sqrt(pow((double)M,2)) //scales FFT
    kint += (tempc.cwiseProduct(tempc.conjugate())).real();
    //variance
    for(int i=1; i<npc;++i){
        tempd= VorEnd(i);
        fft2_eigenxdc(tempd,M,fft_fwd,in,out,tempc);
        tempc/=(double)pow(M,2);
        kinv+= (tempc.cwiseProduct(tempc.conjugate())).real();
    }
    kint += kinv;

    EnergyTotal(Res_cnt)    = kint;
    EnergyVariance(Res_cnt) = kinv;
}



void MainLoopSNS_DetVis(int K, int N,int K0, int N0, int D, int M, int Res_no, int Res_cnt, int Nsamp,
                        int nt, int FreqCut, double Ti, double Tf, MatrixXd& VorInit, double Vis,
                        MatrixXcd& Fdsigma1, MatrixXcd& Fdsigma2, VectorXd& kx, VectorXd& ky,
                        MatrixXd& k2, MatrixXi& rvecxi, MatrixXi& rvecxieta,
                        fftw_plan& fft_fwd, fftw_plan& fft_inv,fftw_complex *in,fftw_complex *out,fftw_complex *inv)
{

    cout<<"DgPC algorithm for SNS with deterministic viscosity starts...."<<endl;

    double int_dt=(Tf-Ti)/(double)(Res_no+1); //restart time-step

    int KD= K+D; // total number of random variables

    //-----------multi-index init
    Array2D<int> mindexu,mindexue,mindexue3;
    long npcu= compute_multi_ind(K0,N0,mindexu); //only xi
    long npcue= compute_multi_ind(KD,N,mindexue); // (xi,eta)
    long npcue3= compute_multi_ind(KD,3*N,mindexue3);
    Array2D<int> wmatue,wmatue2,wmatue3;
    wmatue = weight_mat(KD, N);
    wmatue2 = weight_mat(KD, 2*N);
    wmatue3 = weight_mat(KD, 3*N);
    Array2D<int> mindexusp,mindexuesp,mindexue3sp;
    Array1D<int> indusp,induesp,indue3sp;
    long npcuesp, npcue3sp;
    //-----------END multi-index init

    //---------- Sparse multi-index init
    Vector3Dint rmatxieta(0),rmatxi(0);
    Array2D<int> rvecxi_int;
    eigentouqcopy(rvecxi,rvecxi_int);
    //sparse multiplication in xi
    fill_rmat(rmatxi,rvecxi_int,N0);
   
    multi_ind_mat_sp(mindexu, K0, rvecxi_int,mindexusp,indusp);
    npcu=mindexusp.XSize();
    mindexu=mindexusp;

    Array2D<int> rvecxieta_int;
    eigentouqcopy(rvecxieta,rvecxieta_int); /// just to copy array format DO IT WITH EIGEN
    fill_rmat(rmatxieta,rvecxieta_int,N);

    multi_ind_mat_sp(mindexue, KD, rvecxieta_int,mindexuesp,induesp);
    multi_ind_mat_sp(mindexue3, KD, rmatxieta[2],mindexue3sp,indue3sp);

    npcuesp = mindexuesp.XSize();
    npcue3sp = mindexue3sp.XSize();
    npcue= npcuesp;
    npcue3= npcue3sp;
    mindexue= mindexuesp;
    mindexue3=mindexue3sp;
    //------------sparse multi index DONE

    // Triple Products
    Vector1D TripProdWick(0),TripProdNew(0); // for trips
    Vector2Dint TripIndicesWick(0),TripIndicesNew(0);
    Vector1Dusint TripIndexStored(0);
    // triple products for Wick polynomials
    trip_prod_wick_sp(TripProdWick, mindexu,TripIndicesWick); //first iteration
    int TripProdSzWick = TripIndicesWick.size();


    // offline step where we keep indices for future restarts
    if(Res_no>0)
        trip_prod_cmbnd_preind(npcue,mindexue,wmatue3,indue3sp,TripIndexStored);


    //----- covariance in frequency domain with cut-off
    int CovarianceInFreq=0;
    VectorXi SpIndex,veck;
    int SpIndexSz;
    if(CovarianceInFreq){
        assert(M%FreqCut==0);
        SpIndexSz= (int)M/FreqCut;
        veck.resize(SpIndexSz);
        for(int i=0; i< SpIndexSz/2; ++i){
            veck(i) = i;
            veck(SpIndexSz-1-i) = M-i-1;
        }
        SpIndex.resize((int)SpIndexSz*M);
        SpIndex.setZero();
        for(int i=1; i<= M ;++i){
            SpIndex.segment((i-1)*SpIndexSz, SpIndexSz ) = (veck + (i-1)*VectorXi::Constant(SpIndexSz,M)).transpose();
        }
    }
    //-------

   
    //----initialization vorticity
    ArrMatXd Vor1(npcu,1), VorEndo(npcu,1),VorInitNew(npcue,1),VorEndNew(npcue,1), /// (MxM) x npcue
             MeanVor(Res_no+2,1), VarVor(Res_no+2,1),U3Vor(Res_no+2,1), U4Vor(Res_no+2,1),
             EnergyTotal(Res_no+2,1), EnergyVariance(Res_no+2,1); // E[w^2] in frequency domain

    init_arrmat(MeanVor,Res_no+2,1); init_arrmat(VarVor,Res_no+2,1);
    init_arrmat(U3Vor,Res_no+2,1); init_arrmat(U4Vor,Res_no+2,1);
    init_arrmat(EnergyTotal,Res_no+2,1); init_arrmat(EnergyVariance,Res_no+2,1);

    MeanVor(0) = VorInit;
    init_arrmat(Vor1,npcu,1);
    Vor1(0) = VorInit;
    // ----

    // 1i*kron(ky',ones(1,M))
    MatrixXcd kixmat(M,M), kiymat(M,M);
    kixmat.setZero(); kiymat.setZero();
    for (int i=0; i<M; ++i){
        for(int j=0; j<M; ++j){
            kixmat(i,j).real() =0;
            kixmat(i,j).imag() = kx(j);
            kiymat(i,j).real() =0;
            kiymat(i,j).imag() = ky(i);
        }
    }
    
    
    //---KL arrays
    ArrMatXd EigfuncVor(D,1); // Eigenfunctions
    MatrixXd EtaKLModes(npcu,D),OrthPolyMat;
    VectorXd Eigs(D);
    std::string EigsStr="LM";// largest in magnitude
    //---

    //----arrays for keeping moments 
    VectorXd MomentsXi1d(3*N+1,1);
    gaussian_mom(0,1,3*N,MomentsXi1d);
    VectorXd MeansXi(npcue3);
    VectorXd MeansEta(npcue3);
    MatrixXd MomentsMatEta(3*N+1,D);
    MomentsMatEta.setZero();
    MomentsMatEta.row(0).setOnes();
    VectorXd uemean(npcue);
    MatrixXd EtaSamplesOld(Nsamp,D),EtaSamplesNew(Nsamp,D); //samples for eta
    // multivariate moments of Gaussians
    get_multimeans_xi(mindexue3,K,MomentsXi1d,MeansXi);
    //-----
    
    
    boost::progress_timer Keeptime;
    // TIME EVOLUTION LOOP
    while(Res_cnt <= Res_no){
        // first restart
        if(Res_cnt==0){

            Tf=Ti+int_dt;

            ODEint_SNS_DetVis_PC4(Ti,Tf,Res_cnt,K0, M,npcu,nt,Vis,kixmat,kiymat,k2,Fdsigma1,Fdsigma2, Vor1, TripProdWick,
                                  TripIndicesWick,TripProdSzWick, MeanVor, VarVor, fft_fwd, fft_inv,in,out,inv,VorEndo);

            SetHigherMoments(VorEndo,M,Res_cnt+1,npcu,TripProdWick,TripIndicesWick,TripProdSzWick,U3Vor,U4Vor);
            SetVorticityEnergy_InFreq( M, npcu, Res_cnt+1, VorEndo,fft_fwd,in,out, EnergyTotal, EnergyVariance);

            Ti += int_dt;

        }else{
            
            // Covariance assembly and computation of eigenfunctions 
            if(Res_cnt==1){

                if(CovarianceInFreq){ // frequency domain
                    GetCovarianceEigenfunc_InFreq(VorEndo,  npcu,  M, D,EigsStr, EigfuncVor, Eigs,  SpIndex,  SpIndexSz,
                                            fft_fwd , fft_inv, in,out,inv);
                }else{ //real domain
                     GetCovarianceEigenfunc(VorEndo,npcu,M,D,EigsStr,EigfuncVor,Eigs);
                }
                GetKLModes( VorEndo, Eigs,EigfuncVor, npcu, M, D, EtaKLModes);

                GetSamplesEta(K0,N0, mindexu, EtaKLModes,  EtaSamplesOld);
                MomentsFromData(D,Nsamp,npcue3,mindexue3, EtaSamplesOld,MeansEta,MomentsMatEta);

                EtaKLModes.resize(0,0); //clear

            }else{
                EtaKLModes.resize(npcue,D);

                if(CovarianceInFreq){// frequency domain
                     GetCovarianceEigenfunc_InFreq(VorEndNew,  npcue,  M, D, EigsStr, EigfuncVor, Eigs,  SpIndex,  SpIndexSz,
                                            fft_fwd , fft_inv, in,out,inv);
                }else{// real domain
                    GetCovarianceEigenfunc(VorEndNew,npcue,M,D,EigsStr,EigfuncVor,Eigs);
                }
                GetKLModes( VorEndNew, Eigs,EigfuncVor, npcue, M, D, EtaKLModes);

                GetSamplesEtaAfter(K, D,mindexue,EtaKLModes,EtaSamplesOld, OrthPolyMat, EtaSamplesNew);

                MomentsFromData(D,Nsamp,npcue3,mindexue3, EtaSamplesNew,MeansEta,MomentsMatEta);

                EtaSamplesOld=EtaSamplesNew; /// keep for the next iteration
                EtaSamplesNew.setZero();

            }


            GetOrthonormalPolys( npcue,  MeansXi, MeansEta, mindexue, wmatue3,  indue3sp, OrthPolyMat);

            GetTripleProductSparse( npcue,MeansXi,MeansEta,  OrthPolyMat,mindexue,wmatue,
                        wmatue2, wmatue3, indue3sp,TripIndexStored,TripProdNew, TripIndicesNew, uemean);

            //initials for the next interval
            init_arrmat(VorInitNew,npcue,1);
            if(Res_cnt==1){
                GetInitPCE( VorEndo, K,  M,npcue, D, Eigs, EigfuncVor,
                             OrthPolyMat, MeansXi, MeansEta, mindexue,  wmatue3, indue3sp, VorInitNew);
            }else{
                GetInitPCE( VorEndNew, K,  M,npcue, D, Eigs, EigfuncVor,
                             OrthPolyMat, MeansXi, MeansEta, mindexue,  wmatue3, indue3sp, VorInitNew);
            }

            Tf=Ti+int_dt;

            ODEint_SNS_DetVis_PC4(Ti,Tf,Res_cnt,K,M,npcue,nt,Vis,kixmat,kiymat,k2,Fdsigma1,Fdsigma2, VorInitNew, TripProdNew,
                                  TripIndicesNew,TripIndicesNew.size(), MeanVor, VarVor,
                                  fft_fwd, fft_inv,in,out,inv,VorEndNew);

            SetHigherMoments(VorEndNew,M,Res_cnt+1,npcue,TripProdNew,TripIndicesNew,TripIndicesNew.size(),U3Vor,U4Vor);
            SetVorticityEnergy_InFreq( M, npcue, Res_cnt+1, VorEndNew, fft_fwd,in,out, EnergyTotal, EnergyVariance);

            Ti=Tf;
        }
        Res_cnt+=1;
    }
    // write moments and energy to data files
    WriteDataSNSVor_3dto1d(K,  N, D, M,  Tf,Res_no,  nt, Nsamp,
                         MeanVor, VarVor,  U3Vor, U4Vor, EnergyTotal,EnergyVariance, rvecxieta,
                         Keeptime.elapsed(), CovarianceInFreq, FreqCut);

}

