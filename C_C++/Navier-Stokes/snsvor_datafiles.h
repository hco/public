#ifndef SNSVOR_DATA_H
#define SNSVOR_DATA_H

#include "snsvor_aux.h"
#include <fstream>
#include <string>
#include <Eigen/Dense>


using namespace std;
using namespace Eigen;

/* Data files routines
 * Some codes are taken from UQTk 2.0 
 * 
 * H. Cagan Ozen, June 2015
 */


/// Read a datafile from filename in a matrix form and store it in Matrix
void read_datafile_2d(MatrixXd& , const char* );

/// saves 1d dimensional double vector Eigen format
void write_datafile_1d(const VectorXd& , const char* );
/// saves 1d dimensional integer vector Eigen format
void write_datafile_1d(const VectorXi& , const char* );
/// saves 3dimensional data(nx)(i,j) in a .dat file with dimension n x MxM
void write_datafile_3dto1d(const ArrMatXd&, const char* );
/// write 2d Matrix into dat file
void write_datafile_2d(const MatrixXd&, const char*);

/// setups necessary names and save Moments into dat files
/// for Navier-Stokes equation with deterministic viscosity
void WriteDataSNSVor_3dto1d(int , int , int ,int , double ,int , int , int ,
                        ArrMatXd& , ArrMatXd& , ArrMatXd& , ArrMatXd& ,ArrMatXd&, ArrMatXd& , MatrixXi& ,double  ,int , int );


#endif
