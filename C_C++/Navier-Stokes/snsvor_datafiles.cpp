#include "snsvor_datafiles.h"



void read_datafile_2d(MatrixXd& data, const char* filename)
{
    int nx=data.rows();
    int ny=data.cols();

    if (nx==0 || ny==0){
        printf("read_datafile()    : the requested data array is empty\n") ;
        exit(1) ;
    }
    
    ifstream in(filename);

    if(!in){
        printf("read_datafile()    : the requested file %s does not exist\n",filename) ;
        exit(1) ;
    }

    string theLine="";
    int ix=0;

    while(in.good()){
        getline(in,theLine);

        if (theLine=="") break;
        if ( theLine.compare(0, 1, "#") == 0 ) continue ;

        istringstream s(theLine);
        int iy=0;
        double tmp;
        while(s>>tmp){
            data(ix,iy)=tmp;
            iy++;
            }
            if (iy!=ny) {printf("Error at line %d while reading %s; number of columns should be %d\n", ix+1, filename,ny); exit(1);}
            ix++;
    }
    if (ix!=nx) {printf("Error while reading %s; number of rows should be %d\n", filename,nx); exit(1);}

    in.close();

    return;
}


void write_datafile_1d(const VectorXd& data, const char* filename)
{
    int nx=data.size();

    FILE* f_out;
    if(!(f_out = fopen(filename,"w"))){
        printf("write_datafile: could not open file '%s'\n",filename);
        exit(1);
    }

    for(int ix = 0 ; ix < nx ; ix++) fprintf(f_out, "%24.16lg\n", data(ix));

    if(fclose(f_out)){
        printf("write_datafile: could not close file '%s'\n",filename);
        exit(1);
    }

    #ifdef VERBOSE
        printf("Data written to '%s' in a matrix form [%d X 1]\n", filename,nx);
    #endif

    return;
}

void write_datafile_1d(const VectorXi& data, const char* filename)
{

    unsigned long long nx=data.size();

    FILE* f_out;
    if(!(f_out = fopen(filename,"w"))){
        printf("write_datafile: could not open file '%s'\n",filename);
        exit(1);
    }

    for(unsigned long long ix = 0 ; ix < nx ; ix++) fprintf(f_out, "%d\n", data(ix));

    if(fclose(f_out)){
        printf("write_datafile: could not close file '%s'\n",filename);
        exit(1);
    }

    return;
}


void write_datafile_3dto1d(const ArrMatXd& data, const char* filename)
{
    int M=data(0).rows();
    int nx=data.rows();

    // dim nx x (MxM)

    FILE* f_out;
    if(!(f_out = fopen(filename,"w"))){
        printf("write_datafile: could not open file '%s'\n",filename);
        exit(1);
    }

    for(int ix = 0 ; ix < nx ; ix++){
        for(int i = 0 ; i < M ; i++){
            for(int j = 0 ; j < M ; j++){
                fprintf(f_out, "%24.16lg ", data(ix)(i,j) );
            }
        fprintf(f_out, "\n");
        }
    }

        if(fclose(f_out)){
        printf("write_datafile: could not close file '%s'\n",filename);
        exit(1);
    }

    return ;

}

//sandia
void write_datafile_2d(const MatrixXd& data, const char* filename)
{
    int nx=data.rows();
    int ny=data.cols();


    FILE* f_out;
    if(!(f_out = fopen(filename,"w"))){
        printf("write_datafile: could not open file '%s'\n",filename);
        exit(1);
    }

    for(int ix = 0 ; ix < nx ; ix++){
        for(int iy = 0 ; iy < ny ; iy++){
            fprintf(f_out, "%24.16lg ", data(ix,iy));
        }
        fprintf(f_out, "\n");
    }

    if(fclose(f_out)){
        printf("write_datafile: could not close file '%s'\n",filename);
    exit(1);
    }

    #ifdef VERBOSE
        printf("Data written to '%s' in a matrix form [%d X %d]\n", filename,nx,ny);
    #endif

 return ;

}


void WriteDataSNSVor_3dto1d(int K, int N, int D,int M, double Tf,
                        int Res_no, int nt, int Nsamp,
                        ArrMatXd& MeanVor, ArrMatXd& VarVor, ArrMatXd& U3Vor, ArrMatXd& U4Vor,
                        ArrMatXd& EnTot, ArrMatXd& EnVar,
                        MatrixXi& rvecxieta,double time_elapsed ,int CovarianceInFreq, int FreqCut)
{

    stringstream meanws,varws,u3ws,u4ws,ts, entots, envars;

    vector<int> vec1;
    for(int i=0; i<rvecxieta.cols();++i)
        vec1.push_back(rvecxieta(N,i)); //last row


    std::ostringstream oss1;

    if (!vec1.empty()){
        std::copy(vec1.begin(), vec1.end(),
            std::ostream_iterator<int>(oss1, ""));
    }
    string sxieta= oss1.str();
    
    // filenames: first 4 moments and energy
    meanws<<"snsv_m_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    varws<<"snsv_v_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    u3ws<<"snsv_u3_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    u4ws<<"snsv_u4_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    ts<<"snsv_t_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    entots<<"snsv_entot_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";
    envars<<"snsv_envar_"<<"Tf_"<<Tf<<"nt_"<<nt<<"Res_"<<Res_no<<"M_"<<M<<"K_"<<K<<"N_"<<N<<"D_"<<D<<"S_"<<Nsamp/1000<<"xie_"<<sxieta<<"ck_"<<CovarianceInFreq<<FreqCut<<".dat";

    // \todo with loop
    string str1=meanws.str();
    string str2=varws.str();
    string str3=u3ws.str();
    string str4=u4ws.str();
    string str5=ts.str();
    string str6=entots.str();
    string str7=envars.str();

    const char *cstr1 = str1.c_str();
    const char *cstr2 = str2.c_str();
    const char *cstr3 = str3.c_str();
    const char *cstr4 = str4.c_str();
    const char *cstr5 = str5.c_str();
    const char *cstr6 = str6.c_str();
    const char *cstr7 = str7.c_str();

    write_datafile_3dto1d(MeanVor,cstr1);
    write_datafile_3dto1d(VarVor,cstr2);
    write_datafile_3dto1d(U3Vor,cstr3);
    write_datafile_3dto1d(U4Vor,cstr4);

    VectorXd timevec(1);
    timevec(0) = time_elapsed; //write elapsed time too
    write_datafile_1d(timevec,cstr5);
    write_datafile_3dto1d(EnTot,cstr6);
    write_datafile_3dto1d(EnVar,cstr7);
}







