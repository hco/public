#ifndef SNSVOR_SAMP_H
#define SNSVOR_SAMP_H


#include "auxiliary_uq.h"
#include <Eigen/Dense>
#include <boost/progress.hpp>

using namespace Eigen;



/* Sampling routines
 * Evaluation of polynomials
 * depends on Boost sampling routines
 * 
 * H. Cagan Ozen, June 2015
 */

/// given samples of multivariate random variables, compute moments 
/// using its samples
void MomentsFromData(int , int , int , Array2D<int>& , MatrixXd& , VectorXd& , MatrixXd& );
/// evaluation of  1d dimensional Hermite polynomials H(xi)
void EvalHermite1d(VectorXd& , MatrixXd& ,int );
/// evaluation of Multivariate Hermite polynomials given the multi-index set
void EvalMultiHermite(int  ,int , Array2D<int>& , VectorXd& , VectorXd& );

/// gets samples for KL modes Eta given their PC representations in terms of Gaussians
void GetSamplesEta(const int ,const int , Array2D<int>& , MatrixXd& , MatrixXd& );
/// Evaluates samples of next step EtaKLModes from samples of previous EtaKLModes and new samples
/// of Brownian motion (Gaussian Random Variables) through given PCE representation
void GetSamplesEtaAfter(const int ,const int , Array2D<int>& , MatrixXd& , MatrixXd& , MatrixXd& , MatrixXd& );

/// evaluates [xi xi2].^mindex(i,:)
/// xi Gaussian samples , xi2 Eta samples
void eval_xiboth(int ,int ,VectorXd& ,VectorXd& ,Array2D<int>& ,VectorXd& );
/// Prod (x.^ind) where ind in multi index
double pwind(VectorXd&, Array1D<int>);
/// 1d Normalized Legendre polynomials T(xi)
void EvalLegendre1d(VectorXd& , MatrixXd&, int );
/// Evaluates Multivariate polynomial in Gaussian and Uniform random variables T((xi,U))
void evalMultiHermiteLeg(int ,int , Array2D<int>& , VectorXd&, VectorXd& ,VectorXd& );

/// Evaluates samples of EtaKLModes from samples
/// of Brownian motion (Gaussian Random Variables) through given PCE representation and
/// samples of remaining Uniform random variables
void GetSamplesEtaUniform(const int ,const int , Array2D<int>& , MatrixXd& , MatrixXd& );
















#endif
