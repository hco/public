The repo consists of my selected work during my Ph.D. and M.S. degrees. The contents are grouped under different programming languages. 

Repo contents: 

1. C\C++ 
	a. Stochastic Navier-Stokes: Propagation of stochasticity by using dynamical generalized polynomial chaos expansions (see (http://arxiv.org/abs/1605.04604))

2. Matlab
	a. Scattering/Nystrom  : Nystrom and exact methods to compute scattered wave from kite shaped/circle obstacles 
	b. Scattering/Galerkin : Boundary element/Galerkin method with partition of unity to compute surface density of the combined
							 field integral equation for acoustic wave scattering, see doi:10.1007/s00211-016-0800-7
	 
3. Python 
	a. FEM  : Finite element approximation to exterior wave scattering problem (Helmholtz equation)
	b. DgPC : Stochastic ordinary differential equations solvers using dynamical polynomial chaos expansions (Ornstein-Uhlenbeck type processes),
			  see doi:10.1137/15M1019167

4.  R
	a. ISL : R Labs from the book "An Introduction to Statistical Learning with Applications in R"
	b. ML  : Solutions to homework problems in ML class


