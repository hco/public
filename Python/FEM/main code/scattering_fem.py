"""
Created on Fri Dec 14 00:42:22 2012

@author: H. Cagan Ozen

---- 2D exterior direct wave scattering problem-----
-Solves Helmholtz equation with Sommerfeld boundary condition
	using Finite Element approximation on an annulus
    utilizing an artifical boundary
-h/p convergence 

-FEniCS library required
"""



from dolfin import *
import numpy as np
import scipy.special as sp
import scipy
from matplotlib import pyplot as plt
from time import time


#computational domain 1<=r<=4 (annulus)
# inner
class Inner_bd(SubDomain):
    def inside(self, x, on_boundary):
        tol=1E-4 # turns out that gmsh precision for boundaries is only 1e-4
        return on_boundary and ((x[0]*x[0]+x[1]*x[1]-1)<tol) 
# outer        
class Outer_bd(SubDomain):
    def inside(self, x, on_boundary):
        tol=1E-4
        return on_boundary and ((x[0]*x[0]+x[1]*x[1]-16)<tol) 

'''
k       : wave number for incoming wave
1/h     : characteristic length for gmsh
p       : polynomial degree
a=1     : inner radius, unit circle
b       : outer radius
[d1,d2] : unit direction of propagation
ref     : boolean for refinement in shadow region(back)
'''
def fem_main(k,h,p,a,b,d1,d2,ref):
    
    # mesh name: meshes/annulush.xml , h=2,4,8,16,32 
    filename = 'meshes/annulus{0}.xml'.format(h)
    mesh = Mesh(filename) # import from gmsh
    meshwidth=(mesh.hmax()+mesh.hmin())/2.0
    
	# mesh refinement
    if(ref):
        Pt_sh=Point(2.0,0.0) # assuming (d1,d2)=(1,0)
        markers = MeshFunction("bool", mesh, mesh.topology().dim())
        markers.set_all(False)
        for cell in cells(mesh):
            if cell.midpoint().distance(Pt_sh) < 2.0:
                markers[cell.index()] = True
        # Refine mesh
        mesh = refine(mesh, markers)
    
    
    coor = mesh.coordinates()

    domains = CellFunction("uint", mesh)
    domains.set_all(0)
	
	# set boundary conditions
    bottom = Inner_bd()
    top = Outer_bd()
    #boundaries = FacetFunction("uint", mesh)
    boundaries =MeshFunction("uint", mesh, mesh.topology().dim()-1)
    boundaries.set_all(0)
    top.mark(boundaries, 1)
    bottom.mark(boundaries, 2)
    
    # BC u_scat=-u_inc       
    g = Expression(("-cos(kk*(x[0]*c+x[1]*d))", "-sin(kk*(x[0]*c+x[1]*d))"),kk=k,c=d1,d=d2)

	# init function spaces
    V = FunctionSpace(mesh, "CG",p)
    W = V * V
    
    #Rhs
    fr = Expression("0")
    fi=Expression("0")

    (vr, vi) = TestFunctions(W)
    (ur, ui) = TrialFunctions(W)

    ds = Measure("ds")[boundaries] # use top ds(1) for radiation condition

    # bilinear form with DtN map Mu= (-ik+1/2b)u
    a=inner(grad(ur), grad(vr))*dx+inner(grad(ui), grad(vi))*dx\
     +inner(grad(ui), grad(vr))*dx-inner(grad(ur), grad(vi))*dx\
     -k**2*(ur*vr+ui*vi)*dx\
     -k**2*(ui*vr-ur*vi)*dx\
     -k*(-ui*vr+ur*vi)*ds(1)-k*(ur*vr+ui*vi)*ds(1)\
     +1.0/(2*b)*(ur*vr+ui*vi)*ds(1)+1.0/(2*b)*(ui*vr-ur*vi)*ds(1)
    
    #L=0
    L = inner(fr,vr)*dx - inner(fi, vi)*dx \
        + inner(fr,vi)*dx + inner(fi, vr)*dx

    bc = DirichletBC(W,g, bottom)  # BC on unit circle

	# solve the linear system
    u = Function(W)
    solve(a == L, u, bc)
    
	# if preconditioner wanted, use the commented code below
	# uses PETSc and GMRES
    '''
    u = Function(W)
    problem = LinearVariationalProblem(a, L, u, bc)
    solver = LinearVariationalSolver(problem)
    parameters['linear_algebra_backend']='PETSc'
    prm=parameters['krylov_solver']
    prm['absolute_tolerance'] = 1E-3
    prm['relative_tolerance'] = 1E-6
    prm['maximum_iterations'] = 10000
    solver.parameters["preconditioner"] = "icc"
    solver.parameters["linear_solver"] = "gmres"
    info(parameters, True)
    solver.solve()
    '''
  
    # split solution into real and imag
    ur, ui = u.split(deepcopy=True)
    
    #calculate exact solution at vertices
    utrue_vecr,utrue_veci=exact_sol(k,coor,d1,d2,mesh.num_vertices())

    # interpolate to linear polynomials and calculate the error
    Ve = FunctionSpace(mesh, "CG", 1)
    We = Ve * Ve
    u_e=interpolate(u,We)
    uer, uei = u_e.split(deepcopy=True)

    e_Vr=Function(Ve)
    e_Vi=Function(Ve)
    e_Vr.vector()[:]=uer.vector().array()-utrue_vecr
    e_Vi.vector()[:]=uei.vector().array()-utrue_veci

    et_Vr=Function(Ve)
    et_Vi=Function(Ve)
    et_Vr.vector()[:]=utrue_vecr
    et_Vi.vector()[:]=utrue_veci

    error1= (e_Vi**2+e_Vr**2)*dx
    error2= (et_Vi**2+et_Vr**2)*dx
    
    rel_err=np.sqrt(assemble(error1))/np.sqrt(assemble(error2))
    print "k:",k," /  mesh width:",round(meshwidth,3)," / relative L2 error:",rel_err
    return rel_err

# compute exact solution of
# the scatttered wave on vertices
def exact_sol(k,coor,d1,d2,num_ver):
    sz=num_ver
    N=200.0
    mvec=np.arange(-N,N+1, dtype=float)
    Mat_han=sp.hankel1(np.arange(-N,N+1, dtype=float),k)
    a0=0
    b0=2*np.pi
    n=128
    l=2*n
    h=(b0-a0)/l

    # set of knots t_i=pi*(i)/n j=0,..,2n-1
	# compute exact surface density
    t=np.linspace(a0,b0-h,l)
    exact_den=1j*np.zeros((2*n,1))
    for j in range(0,2*n):
        exact_den[j]=-2*1j/(np.pi)*np.sum(np.exp(1j*mvec*(t[j]+np.pi/2))/Mat_han)

    x1=np.cos(t)
    x2=np.sin(t)
    dx1=-x2
    dx2=x1
    Y=np.sqrt(dx1**2+dx2**2)
    scat=1j*np.zeros((sz,1)) 
    # compute scattering wave 
    for i in range(sz):
        if (coor[i][0]**2+coor[i][1]**2-1)<1E-4: # 1-e4 resolution for boundary points
            scat[i]=-np.cos(k*(coor[i][0]*d1+coor[i][1]*d2))-1j*np.sin(k*(coor[i][0]*d1+coor[i][1]*d2))
        else:
            scat[i]=compute_quad(k,n,coor[i],x1,x2,exact_den,Y)

    tempr = scipy.zeros(sz)
    tempi = scipy.zeros(sz)
    
    for i in range(sz):
        tempr[i] = scat.real[i][0]
        tempi[i] = scat.imag[i][0]
    
    return tempr, tempi

# quadrature for the integral (periodic Trapezoidal rule)
def compute_quad(k, n, coor, x1, x2, exact, Y):
    dist=np.zeros((2*n,1))
    for i in range(0,2*n):
        dist[i]=np.sqrt((coor[0]-x1[i])**2+(coor[1]-x2[i])**2)
    ret=0.0*1j
    for i in range(0,2*n):
        ret+=-(np.pi/n)*(1j/4.0)*sp.hankel1(0,k*dist[i])*exact[i]*Y[i]
    return ret



def main():
    # annulus parameters
    a, b = 1, 4
    k = 200
    # unit direction
    d1, d2 = 1, 0
    itmax = 4
    bool_hconv = True
    show_error = True

    # containers for error and timing
    tvec = np.zeros((itmax, 1))
    err_vec1 = np.zeros((itmax, 1))
    err_vec2 = np.zeros((itmax, 1))

    if bool_hconv:
        # h convergence
        p=5
        h=2

        hvec=np.zeros((itmax,1))
        while h<2**(itmax+1):
            tic=time()
            err_vec1[np.log2(h)-1]=fem_main(k,h,p,a,b,d1,d2,False)
            err_vec2[np.log2(h)-1]=fem_main(k,h,p,a,b,d1,d2,True)
            hvec[np.log2(h)-1]=1./(h)
            tm=time()-tic
            tvec[np.log2(h)-1]=tm
            h=h*2

        title='k='+str(k)+' -- '+ 'p='+str(p)
        if show_error:
           # plt.figure()

            plt.loglog(hvec,err_vec1,'-o',hvec,err_vec2,'-*',linewidth=2.5,markeredgewidth=2.0)
            plt.grid(True,which="both")
            plt.legend(['L2_err','L2_err_ref'],loc="best")
            plt.xlabel('h')
            plt.title(title)
            plt.ylabel('||e||_2')
            # ax.xaxis.set_ticks([1.,2.,3.,10.]
            # plt.savefig('err_plot2')
            plt.show()
    else:
        # p  convergence
        p=11
        h=4
        pvec=np.zeros((itmax,1))

        while p>=1:
            tic=time()
            err_vec1[p/2]=fem_main(k,h,p,a,b,d1,d2,False)
            pvec[p/2]=p
            tm=time()-tic
            tvec[p/2]=tm
            p=p-2

        title='k='+str(k)+' -- '+ 'h='+str(h)
        if show_error:
            #plt.figure()
            plt.loglog(pvec,err_vec1,'-o',linewidth=2.5,markeredgewidth=2.0)
            plt.grid(True,which="both")
            plt.legend(['L2_err'],loc="best")
            plt.xlabel('p')
            plt.title(title)
            plt.ylabel('||e||_2')
            #ax.xaxis.set_ticks([1.,2.,3.,10.]
            plt.savefig('err_plot2')
            plt.show()
    

if __name__ == '__main__':
    main()




