# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 11:35:15 2013

modified Thu Apr 2015

@author: Cagan Ozen

Solve Ornstein-Uhlenbeck Process using Dynamical Polynomial
Chaos Expansions

"""


import numpy as np
import matplotlib.pyplot as pl
import multi_index_class as mi
import orth_poly_class as op
from scipy import linalg as ln
#import cProfile
#import time



# ode integration before restart
def odeint_OU_b(b, sigma, Ti, Tf, K, N, npcu, nt, u1,Type,mean,var):

    dt= (Tf-Ti)/(float(nt))
    unext=np.zeros(npcu)

    if Type=="Euler":
        t1 = Ti
        for it in xrange(1, nt + 1):
            for k in xrange(0, npcu):
                if k>=1 and k<npcu: # first order polynomials
                    term = -b * u1[k] + sigma * op.cal_mif(k, t1 , Ti, Tf)
                else:
                    term = -b * u1[k]

                unext[k] = u1[k] + dt * term

            t1 += dt
            u1 = unext
            mean[it] = unext[0]
            var[it]= np.sum(unext[1:]**2)

    elif Type=="Mid":

        t1 = Ti
        for it in xrange(1, nt + 1):
            for k in xrange(0, npcu):
                if k>=1 and k<npcu: # first order polynomials
                    temp = u1[k] + (dt / 2) * (-b * u1[k] + sigma * op.cal_mif(k, t1, Ti, Tf))
                    term = -b * temp + sigma * op.cal_mif(k, t1 + dt / 2, Ti, Tf)
                else :
                    term = -b * (u1[k] + dt / 2 * (-b * u1[k])) # Midpoint rule

                unext[k] = u1[k] + dt * term

            t1 += dt
            u1 = unext
            mean[it] = unext[0]
            var[it]= np.sum(unext[1:]**2)

    elif "RK4":
        t1 = Ti
        for it in xrange(1, nt + 1):
            for k in xrange(0, npcu):
                if k>=1 and k<npcu: # first order polynomials
                    k1= dt*(-b*u1[k]+sigma* op.cal_mif(k, t1, Ti, Tf))
                    k2= dt*(-b*(u1[k]+dt/2.0*k1)+sigma* op.cal_mif(k, t1+dt/2.0, Ti, Tf))
                    k3= dt*(-b*(u1[k]+dt/2.0*k2)+sigma* op.cal_mif(k, t1+dt/2.0, Ti, Tf))
                    k4= dt*(-b*(u1[k]+dt*k3)+sigma* op.cal_mif(k, t1+dt, Ti, Tf))
                else :
                    k1= dt*(-b*u1[k])
                    k2= -b*dt*(u1[k]+dt/2.0*k1)
                    k3= -b*dt*(u1[k]+dt/2.0*k2)
                    k4= -b*dt*(u1[k]+dt*k3)

                unext[k] = u1[k] + 1.0/6.0 * (k1+2*k2+2*k3+k4)
            t1 += dt
            u1 = unext
            mean[it] = unext[0]
            var[it]= np.sum(unext[1:]**2)


    return unext

# ode integration after restart
def odeint_OU_a(b, sigma, Ti, Tf, K, N, L,Res_cnt, npcu,npcb, u1, psimean, nt, mean, var):

    unext=np.zeros((npcu,npcb)) # 2dim row \xi's column u_j
    dt= (Tf-Ti)/(float(nt))

    t1=Ti
    for it in xrange(1,nt+1):

        #Euler method
        for j in xrange(0,npcu):
            for m in xrange(0,npcb):
                if(j ==0 or j>K):
                    term = -b*u1[j,m]
                else:
                    term = -b*u1[j,m] + sigma* op.cal_mif(j,t1,Ti,Tf)*psimean[m]

                unext[j,m] = u1[j,m] + dt*term

        t1 += dt
        u1 = unext
        mean[it+Res_cnt*nt] = unext[0,0]
        var[it+Res_cnt*nt]= np.sum(unext**2)-unext[0,0]**2


    return unext


# loop main function
def main_func_loop(b, sigma, Ti, Tf, K, N, L, nt, Res_no, meanu0, stdu0):

    int_dt = (Tf - Ti) / (Res_no + 1) # split intervals
    Res_cnt = 0

    mean = np.zeros((Res_no+1)*nt+1)
    var =  np.zeros((Res_no+1)*nt+1)
    mean[0] =meanu0
    var[0] = stdu0**2

    Kuv=1 # rv after restart

    # terms for poly. in xi
    npcu,mindexu = mi.multi_ind_mat(K, N)
    # terms for poly in u
    npcb,mindexb= mi.multi_ind_mat(Kuv, L)

    uinit= np.zeros(npcu)
    mom_vecu = np.zeros(3*L+1)
    mom_vecu[0] = 1.0


    trip_Wick,ijk_sp= op.trip_prod_wick_sp(npcu,mindexu,N)
    trip_u=[]
    lmo_sp=[]

    while (Res_cnt < Res_no + 1):

        if Res_cnt == 0:

            Tf= Ti+int_dt
            uinit[0] = meanu0
            uinit[1] = stdu0

            uendo = odeint_OU_b(b, sigma, Ti, Tf, K, N, npcu, nt, uinit,"Euler",mean,var)

            Ti += int_dt

        else:
            #calculate moments of u
            if Res_cnt == 1: # first restart
                op.cal_mom_b(npcu,uendo,trip_Wick,ijk_sp,mom_vecu)
            else:
                op.cal_mom_a(npcu,npcb,uendo,trip_Wick,ijk_sp,trip_u,lmo_sp,mom_vecu)

            np.set_printoptions(precision=6)
            print 'Moments:',  mom_vecu[1:]
            Hu = ln.hankel(mom_vecu[:L+1],  mom_vecu[L:2*L+1])
            Fu = op.orth_bas_chol(Hu)

            uinita= op.cal_inits_1d(Fu,mom_vecu,npcu,npcb)
            trip_u,lmo_sp,psimean= op.trip_prod_1d(mom_vecu,Fu,npcb)

            Tf = Ti + int_dt

            uendn= odeint_OU_a(b, sigma, Ti, Tf, K, N, L,Res_cnt, \
                               npcu, npcb, uinita,psimean, nt,mean,var)
            Ti = Tf

            uendo= uendn

        Res_cnt += 1

    return mean, var


def main():
    """
             Ornstein-Uhlenbeck equation
             du = -b u ds+ sigma dW

             b : decay
             sigma : noise amplitude
             K : dimension in xi (dW)
             N : degree of polynomial is xi
             L : degree of polynomial in u
             Ti, Tf : initial and final time
             nt : number of time steps
             Res_no : number of restarts
    """

    b, sigma = 2.0, 2.0
    K, N, L = 5, 1, 1
    Ti, Tf, nt, Res_no = 0.0, 3.0, 5000, 14

    # initial mean and stdev
    meanu0, stdu0 = 1, 0

    # cProfile.run('main_func_loop(b, sigma, Ti, Tf, K, N, L, nt, Res_no, meanu0,stdu0)')
    # run the main program
    mean, var = main_func_loop(b, sigma, Ti, Tf, K, N, L, nt, Res_no, meanu0, stdu0)

    # plot  variance
    dt = (Tf - Ti) / float((Res_no + 1) * nt)
    t = np.arange(0, Tf + dt / 2, dt)  # 0,...Tf
    exact_mean = np.exp(-b * t) * 1
    exact_var = (1.0 - np.exp(-2 * b * t)) * sigma ** 2. / (2 * b) + stdu0 ** 2 * np.exp(-2 * b * t)

    pl.plot(t, var, 'r', t, exact_var, 'k')
    pl.ylim(np.min(var) - 0.05, np.max(var) + 0.1)
    pl.xlim(0 - 0.1, Tf + 1)
    pl.legend(('N=%s,K=%s' % (N, K), 'exact'), loc='lower right')
    pl.title('variance')
    pl.show()


if __name__ == '__main__':
    main()








