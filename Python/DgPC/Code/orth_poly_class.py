

import numpy as np
import multi_index_class as mi
from scipy import linalg as ln
from scipy import special as sp


"""
__author__ = @author: H. Cagan Ozen

Orthogonal Polynomials and Moments class


basic routines:
    create orthogonal basis
    computes triple products
"""



def fact(n):
    return np.exp(sp.gammaln(n+1))

# Cholesky of Hankel matrix
# returns orthogonal polynomials
def orth_bas_chol(Hu):
    R=ln.cholesky(Hu,lower=False)
    ln.inv(R,True)
    Fu=np.transpose(R)  #store in row format

    return Fu # orthogonal polynomials


# triple products for 1d random variable u
# using orthogonal polynomials
# store in sparse format
def trip_prod_1d(mom_vecu,Fu,npcb):
    # one d : no need for mindex
    tol=1E-6
    trip_u=[]
    klm_sp=[]

    psimean=np.zeros(npcb)
    for k in xrange(0,npcb):
        for kk in xrange(0,k+1):
            psimean[k]+=mom_vecu[kk]*Fu[k,kk]

    psimeansq=np.zeros(npcb)
    #first calculate meansq
    for k in xrange(0,npcb):

        for kk in xrange(0,k+1):
            for ll in xrange(0,k+1):
                psimeansq[k]+= mom_vecu[kk+ll]*Fu[k,kk]*Fu[k,ll]

    # store if triple products > TOL
    for k in xrange(0,npcb):
        for l in xrange(0,npcb):
            for m in xrange(0,npcb):

                sm=0
                for kk in xrange(0,k+1):
                    for ll in xrange(0,l+1):
                        for mm in xrange(0,m+1):
                            sm+= mom_vecu[kk+ll+mm] * Fu[k,kk]*Fu[l,ll]*Fu[m,mm]

                if(np.abs(sm)>tol):
                    klm_sp.append([k,l,m])
                    trip_u.append(sm)

    return trip_u, klm_sp,psimean


# sparse product of hermite expansion
# ijk_sp indices
# trip_wick : E[T_i(xi) T_j(xi) T_k(xi)]
def prod_trip(u,v,uv,trip_Wick,ijk_sp):
       # writes to uv
        uv[:]=0
        n=0
        for x in ijk_sp:
            i,j,k= x[0],x[1],x[2]
            uv[i] += u[j]*v[k]*trip_Wick[n]
            n+=1

# sparse product of expansions after restart
# trip_u : triple products of u_j E[T_l(u_j) T_m(u_j) T_o(u_j)]
# lmo_sp : sparse index of triple products of u_j
def prod_trip_a(u,v,uv,trip_Wick,ijk_sp,trip_u,lmo_sp):
        # writes to uv be careful
        uv[:]=0
        nn=0
        for x in ijk_sp:
            i,j,k= x[0],x[1],x[2]
            mm=0
            for y in lmo_sp:
                l,m,o=y[0],y[1],y[2]
                uv[i,l] += u[j,m]*v[k,o]*trip_Wick[nn]*trip_u[mm]
                mm+=1
            nn+=1


# triple products for 1D Hermite polynomials
# corresponding to Gaussian distribution
def hermite_trip_1d(i,j,k):

    s= (i+j+k)//2

    if(s<i or s<j or s<k or (i+j+k)%2==1):
        return 0.0
    else :
        # i!j!k! /((s-i)! (s-j)! (s-k)!)
        # exp(log(gamma)) implementation
        return np.exp(sp.gammaln(i+1)+sp.gammaln(j+1)+sp.gammaln(k+1) - \
                       sp.gammaln(s-i+1)-sp.gammaln(s-j+1)- sp.gammaln(s-k+1))

# triple products for Wick polynomials
# sparse index
def  trip_prod_wick_sp(npc,mindex,N):
    # normalized trip products
    '''
    room for improvement, don't call fact too many times, initialize beforehand
    '''

    tol=1E-5
    trip_w=[]
    ijk_sp=[]
    K= np.shape(mindex)[1]

    for i in xrange(0,npc):
        for j in xrange(0,npc):
            for k in xrange(0,npc):

                sm=1.0
                for n in xrange(0,K):
                    sm*= hermite_trip_1d(mindex[i,n],mindex[j,n],mindex[k,n])/ \
                       np.sqrt((fact(mindex[i,n])*fact(mindex[j,n])*fact(mindex[k,n])))

                if(sm>tol):
                    ijk_sp.append([i,j,k])
                    trip_w.append(sm)

    return trip_w,ijk_sp


# calculate cosine basis
def cal_mif(i, t, Ti, Tf):
    if i == 1:
        return 1.0 / np.sqrt(Tf - Ti)
    else:
        return np.sqrt(2.0 / (Tf - Ti)) * np.cos(((i - 1) * np.pi * (t - Ti)) / (Tf - Ti))

# compute initial conditions
def cal_inits_1d(Fu,mom_vecu,npcu,npcb):
    uinita=np.zeros((npcu,npcb))

    for k in xrange(0,npcb):

        for kk in xrange(0,k+1):
            uinita[0,k] += mom_vecu[kk+1]*Fu[k,kk]

    return uinita


# calculates moments at the first restart
def cal_mom_b(npcu,uendo,trip_Wick,ijk_sp,mom_vecu):

    utemp=uendo

    utemp2=np.zeros(npcu)
    mom_vecu[1] = uendo[0]
    sz= np.shape(mom_vecu)[0]

    for j in xrange(2,sz):
        prod_trip(uendo,utemp,utemp2,trip_Wick,ijk_sp)
        utemp = [x for x in utemp2] # copy temp2 to temp without ref to same object
        mom_vecu[j] = utemp2[0]

# calculates moments after the first restart
def cal_mom_a(npcu,npcb,uendo,trip_Wick,ijk_sp,trip_u,lmo_sp,mom_vecu):

    utemp=uendo

    mom_vecu[1] = uendo[0,0]
    sz= np.shape(mom_vecu)[0]
    for j in xrange(2,sz):
        utemp2=np.zeros((npcu,npcb)) #fix later
        prod_trip_a(uendo,utemp,utemp2,trip_Wick,ijk_sp,trip_u,lmo_sp)
        utemp = utemp2[:]
        mom_vecu[j] = utemp2[0,0]