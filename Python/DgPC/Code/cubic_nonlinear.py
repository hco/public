"""
Created on Wed Sep 11 11:35:15 2013

@author: H. Cagan Ozen

Solves OU type equation with cubic nonlinearity
and uses dynamical polynomial chaos expansion


"""


import numpy as np
import multi_index_class as mi
import orth_poly_class as op
from scipy import linalg as ln
import pylab as pl
import cProfile

# ode integration using Euler's method
def odeint_cubic_b_trip(sigma,Ti,Tf,K,nt,npcu,td, u1,trip_Wick,ijk_sp,mean,var):

        u2=np.zeros(npcu)
        u3=np.zeros(npcu)
        unext=np.zeros(npcu)

        dt= (Tf-Ti)/(float(nt))

        op.prod_trip(u1,u1,u2,trip_Wick,ijk_sp) # writes to u2
        op.prod_trip(u2,u1,u3,trip_Wick,ijk_sp) # writes to u3

        t1=Ti
        for it in xrange(1,nt+1):

            # Euler for now
            for j in xrange(0,npcu):
                if(j ==0 or j>K):
                    term = -u3[j] + td*u1[j]
                else:
                    term = -u3[j] + td*u1[j] + sigma* op.cal_mif(j,t1,Ti,Tf)

                unext[j] = u1[j] + dt * term

            t1 += dt
            u1 = unext
            mean[it] = unext[0]
            var[it]= np.sum(unext[1:]**2)

            op.prod_trip(unext,unext,u2,trip_Wick,ijk_sp)
            op.prod_trip(u2,unext,u3,trip_Wick,ijk_sp)


        return unext

# ode integration using Euler's method
def odeint_cubic_a_trip(sigma,Ti,Tf,K,nt,Res_cnt,npcu,npcb,td,u1, \
                        trip_Wick,ijk_sp,trip_u,lmo_sp,psimeans,mean,var):

    u2=np.zeros((npcu,npcb))
    u3=np.zeros((npcu,npcb))
    unext=np.zeros((npcu,npcb))

    dt= (Tf-Ti)/(float(nt))

    op.prod_trip_a(u1,u1,u2,trip_Wick,ijk_sp,trip_u,lmo_sp)
    op.prod_trip_a(u2,u1,u3,trip_Wick,ijk_sp,trip_u,lmo_sp)


    t1=Ti
    for it in xrange(1,nt+1):

        for j in xrange(0,npcu):
            for m in xrange(0,npcb):
                if(j ==0 or j>K):
                    term = -u3[j,m] + td*u1[j,m]
                else:
                    term = -u3[j,m] + td*u1[j,m] + sigma*op.cal_mif(j,t1,Ti,Tf)*psimeans[m]

                unext[j,m] = u1[j,m] + dt*term

        t1 += dt
        u1 = unext
        mean[it+Res_cnt*nt] = unext[0,0]
        var[it+Res_cnt*nt]= np.sum(unext**2)-unext[0,0]**2

        op.prod_trip_a(unext,unext,u2,trip_Wick,ijk_sp,trip_u,lmo_sp)
        op.prod_trip_a(u2,unext,u3,trip_Wick,ijk_sp,trip_u,lmo_sp)

    return unext


def main_func_loop(sigma, Ti, Tf, K, N, L, nt, Res_no,meanu0,stdu0,td):

    int_dt = (Tf - Ti) / (Res_no + 1) # split intervals
    # no of coefficients , same for each sub interval
    Res_cnt = 0

    mean = np.zeros((Res_no+1)*nt+1)
    var =  np.zeros((Res_no+1)*nt+1)
    mean[0] =meanu0
    var[0] = stdu0**2

    Kuv=1 # rv after restart

    npcu,mindexu = mi.multi_ind_mat(K, N)
    npcb,mindexb= mi.multi_ind_mat(Kuv, L)

    uinit= np.zeros(npcu)
    mom_vecu = np.zeros(3*L+1)
    mom_vecu[0] = 1.0

    # triple products of wick polynomails
    trip_Wick,ijk_sp= op.trip_prod_wick_sp(npcu,mindexu,N)
    trip_u=[] #can ignore or delete
    lmo_sp=[]

    while(Res_cnt <= Res_no):

        if(Res_cnt==0):

            Tf= Ti+int_dt
            uinit[0] = meanu0
            uinit[1] = stdu0
            # writes to mean and var
            uendo=odeint_cubic_b_trip(sigma,Ti,Tf,K,nt,npcu,td, uinit, trip_Wick,ijk_sp,mean,var)

            Ti+=int_dt
        else:

            if(Res_cnt==1):
                op.cal_mom_b(npcu,uendo,trip_Wick,ijk_sp,mom_vecu)
            else:
                op.cal_mom_a(npcu,npcb,uendo,trip_Wick,ijk_sp,trip_u,lmo_sp,mom_vecu)

            np.set_printoptions(precision=6)
            print "Moments:", mom_vecu
            Hu = ln.hankel(mom_vecu[:L+1],  mom_vecu[L:2*L+1])
            Fu= op.orth_bas_chol(Hu) #writes FU

            trip_u,lmo_sp,psimean= op.trip_prod_1d(mom_vecu,Fu,npcb)

            uinita= op.cal_inits_1d(Fu,mom_vecu,npcu,npcb)

            Tf= Ti+ int_dt
            uendn = odeint_cubic_a_trip(sigma,Ti,Tf,K,nt,Res_cnt, npcu,npcb,td,uinita, \
                                        trip_Wick,ijk_sp,trip_u,lmo_sp,psimean,mean,var)

            print "----------------------------------------------------------- time",Tf
            Ti =Tf
            uendo=uendn


        Res_cnt+=1


    return mean, var


def main():
    """
             equation with cubic nonlinearity
             u' = -u^3 + u + sigma sum_{j=1}^K xi_j m_j(s),
             where xi_j =N(0,1), m_j basis for L^2

             sigma : noise amplitude
             K : dimension in xi (dW)
             N : degree of polynomial is xi
             L : degree of polynomial in u
             Ti, Tf : initial and final time
             nt : number of time steps
             Res_no : number of restarts
    """

    sigma = 2.0
    meanu0 = 1.0
    stdu0 = 0
    Ti, Tf, nt = 0.0, 4, 500
    Res_no = 19
    K, N, L = 4, 2, 3
    b = 1

    # mean, var= cProfile.run('main_func_loop( sigma, Ti, Tf, K, N, L, nt, Res_no, meanu0,stdu0,td)')
    mean, var = main_func_loop(sigma, Ti, Tf, K, N, L, nt, Res_no, meanu0, stdu0, b)

    # plot variance
    dt = (Tf - Ti) / float((Res_no + 1) * nt)
    t = np.arange(0, Tf + dt / 2, dt)  # 0,...Tf

    pl.plot(t, var, 'b')
    pl.show()

if __name__ == '__main__':
   main()


