import numpy as np
from math import factorial
from math import floor
from scipy import misc,stats
from scipy.special import gammaln


"""
__author__ = @author: H. Cagan Ozen

Multi-index class

basic routines:
    create multi-index set
    return the index of a multi-index
    find multi-indices smaller than given multi-index
    multi-index version of choose(n,k)

"""




def choose(n, k):
    """
    A fast way to calculate binomial coefficients by Andrew Dalke (contrib).
    """
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in xrange(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0

# create multi index matrix
# given the dimension K
# and the degree N
def multi_ind_mat(K, N):
    nc = choose(K + N, N)
    alp = np.zeros((nc, K))

    # N=0
    alp[0] = np.zeros((1, K))

    # N=1
    alp[1:K + 1] = np.eye(K)
    if N == 1:
        return nc, alp

    # N=2
    n, j = 2, 0
    for i in xrange(K, 0, -1):
        v = np.zeros((i, K))
        v[:, K - i] = np.ones((1, i))
        alp[K + 1 + j:K + j + i + 1, :] = alp[K - i + 1:K + 1, :] + v
        j = j + i
    if N == 2:
        return nc, alp

    # N>=3
    inback = np.zeros((K, 1))
    for j in xrange(1, K + 1):
        inback[j - 1] = choose(K + (n - 2) - (j - 1), n - 1)
    indback = choose(K + n - 1, K) + 1
    inback = [int(x) for x in inback]

    for n in xrange(3, N + 1):
        in_ = np.zeros((K, 1))
        for j in xrange(1, K + 1):
            in_[j - 1] = choose(K + (n - 2) - (j - 1), n - 1)
        ind = choose(K + n - 1, K) + 1
        in_ = [int(x) for x in in_]

        j, jback = 0, 0
        for i in xrange(1, K + 1):
            v = np.zeros(((in_[i - 1]), K))
            v[:, i - 1] = np.ones((1, (in_[i - 1])))
            alp[ind + j - 1:ind + j + in_[i - 1] - 1, :] = \
                alp[indback + jback - 1:indback + jback + in_[i - 1] - 1, :] + v
            j = j + in_[i - 1]
            jback = jback + inback[i - 1]
        inback = in_
        indback = ind

    return nc, alp.astype(int)

# given multi_ind find all multi-indices
# smaller than multi_ind
def find_less_vec(multi_ind,k):
    ## k is the kth row in multi ind matrix
    K=np.size(multi_ind,1)
    temp=np.kron(np.ones((k+1,1)), multi_ind[k])
    temp=temp-multi_ind[:k+1]
    vec=np.sum(temp>=0,axis=1)
    vec3= [i==K for i in vec]
    vec2=np.zeros(np.sum(vec3))
    cnt=0
    for i in range(0,np.size(vec)):
         if vec[i]==K:
            vec2[cnt]=i
            cnt+=1

    return vec2

# weight matrix to find the index of a single multi-index
# in the multi-index set
def weight_mat(K, N):
    w_mat = np.zeros((N, K))
    for j in range(1, K + 1):
        for i in range(1, N + 1):
            w_mat[i - 1, j - 1] = choose(K - j + i - 1, K - j)

    for j in range(1, K):
        w_mat[:, j] = w_mat[:, j] + w_mat[:, j - 1]

    return w_mat

# returns the index in multi index set
def ret_ind(alp, w_mat):

    cnt = np.sum(alp)
    if cnt > np.size(w_mat, 0) or not np.all(alp>=0): # check <= N
        return -1

    if cnt == 0:
        return 0
    elif cnt == 1:
        vec=alp.nonzero() #find nonzero entry
        return int(vec[0])+1
    else:

        K = np.size(w_mat, 1)

        ret = 0
        for i in range(0, K):

            while alp[i] > 0:
                ret = ret + w_mat[cnt - 1, i]
                alp[i] -= 1
                cnt -= 1

            cnt = np.sum(alp)
            if cnt == 0: break

    return int(ret)


# UQ toolkit Sandia National Labs version
# get the index
def get_index(alp,N):
   # nd=mi.shape
    nd= np.size(alp)
    ss=np.sum(alp)
    if ss>N or  not np.all(alp>=0):
        return -1

    if ss==0 :
        return 0
    elif ss==1:
        vec=alp.nonzero() #find nonzero entry
        return int(vec[0])+1
    else:

        index=computeNPCTerms(nd,ss-1)
        index+= get_index_ord(alp)
        return index

# UQ toolkit Sandia National Labs
def get_index_ord(mi):
    nd=np.shape(mi)[0]
    mi=[int(x) for x in mi]
    index=0
    ss= np.sum(mi)
    if nd>1 :
         for i in range(ss,mi[0],-1):
            index+= computeNPCTerms(nd-2,ss-i)
         mic=mi[1:]
         index+=get_index_ord(mic)
    return index


# number of terms in PC expansion
def computeNPCTerms(K,N):
    return choose(int(K+N),int(N))

# multi index factorial
def vec_fact(alp):
    # factorial vect
    return [factorial(i) for i in alp]

# multi index choose(a,b) with exp(loggamma) implementation
def multiind_choose(alp,bet):

    K=np.shape(alp)[0]
    tmp =alp-bet

    sm=1.0;
    for i in xrange(0,K):
        sm*=np.exp( gammaln(alp[i]+1) - gammaln(tmp[i]+1) - gammaln(bet[i]+1) )

    return sm

# calculate C(\alpha, \beta, p)
def cal_C(a, b, p):

        a1= multiind_choose(a,b)
        a2= multiind_choose(b+p,p)
        a3= multiind_choose(a-b+p,p)

        return np.exp(0.5*(np.log(a1)+np.log(a2)+np.log(a3)))

# integer version of C(\alpha, \beta, p)
def cal_C_int(j, l, m):
    return np.sqrt(choose(j, l) * choose(l + m, m) * choose(j - l + m, m))

