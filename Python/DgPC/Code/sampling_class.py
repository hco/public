
"""

Created on Tue Feb 04 20:44:06 2014
__author__ = @author: H. Cagan Ozen

Sampling routine class

basic routines:
    evaluate Hermite polynomials
    Sample a PC expansion
    Kernel density estimation for PCE
    compute moments from samples
"""


import numpy as np
from scipy import stats


# evaluate 1D Hermite polynomials
def uq_psi(xi,N):
    nclp = np.size(xi);
    psi = np.zeros((nclp, N + 1));


    psi[:, 0] = 1;

    if N > 0:
        psi[:, 1] = xi;
        for i in xrange(2,N+1):
            psi[:, i] = xi[:] * psi[:, i-1] - (i-1) * psi[:, i-2];

    return psi

# multi dimensional Hermite  polynomials
def uq_PCBasis(K,N,mindex,xi):
    psi=uq_psi(xi,N)
    npc= np.size(mindex,0)
    Psi=np.ones(npc)

    for i in xrange(0,K):
        Psi=Psi*psi[i,mindex[:,i].astype(int)]

    return Psi

# Sample aussian distributions
# and Evaluate PC object
def uq_sample(K,N,mindex,pce,Nsamp):

    U= np.zeros(Nsamp)

    for i in xrange(0,Nsamp):

        xi = np.random.randn(K) #evaluation point

        Psi = uq_PCBasis(K,N,mindex,xi) # multi dimensional basis

        U[i] = np.sum(pce*Psi);

    return U

# Kernel density estimation 1D random variable
def uq_pdf(U,n,lb,rb):
    avg=np.mean(U)
    sig=np.std(U)

    m=avg-lb*sig
    M=avg+rb*sig

    kernel=stats.gaussian_kde(U)

    dx=(M-m)/n
    x=np.arange(m,M+dx/2,dx)


    return kernel.evaluate(x),x


# find moments from samples
def uq_mom_rv(U,Nsamp,L,mom):

    for i in range(1,3*L+1):
        mom[i] = np.sum(U**i)/Nsamp

    return mom

# composite Simplson rule quadrature weights
def simp_weights(x,n):
    h=(x[-1]-x[0])/(n);
    w=np.ones(n+1);
    for j in xrange(1,n):
        w[j] = 3-(-1)**j
    w=h/3*w
    return w




