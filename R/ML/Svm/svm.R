# H. Cagan Ozen
# Nov 2016


library(e1071)
set.seed(100)

train.5<-read.csv("train.5.txt", header=F)
train.6<-read.csv("train.6.txt", header=F)

xtrain=rbind(as.matrix(train.5), as.matrix(train.6))
# 5 --> -1 and 6 --> +1
ytrain = as.factor(rep(c(-1,+1), c(nrow(train.5), nrow(train.6) )))

n= nrow(xtrain)
ind.test = sample(1:n, n/5)

xtest = as.data.frame(xtrain[ind.test, ])
ytest = as.factor(ytrain[ind.test])
xtrain = as.data.frame(xtrain[-ind.test, ])
ytrain = as.data.frame(as.factor(ytrain[-ind.test ]))
names(ytrain) <- "digit"
trainset = cbind(ytrain, xtrain)

#------------------- Training ----------------------


#----------- linear kernel------------------#

# grid search for cost 
svmlinear.tune <- tune.svm(digit ~ ., data = trainset, kernel = "linear", cost = 10^(-5:3) ,scale= FALSE )
# best cost parameter
cost.best = as.numeric(svmlinear.tune$best.parameters)
# train with best cost
svmlinear.model <- svm(digit ~ ., data = trainset, kernel = "linear", cost =cost.best)
plot(svmlinear.tune$performances$cost, svmlinear.tune$performances$error,log='x',
     xlab="cost (margin)", ylab="cross-val error", pch=20, type ='b')
# predictions
svmlinear.pred <- predict(svmlinear.model, xtest)
table(pred =svmlinear.pred, true = ytest)
mean(svmlinear.pred!=ytest)

# ------------radial kernel-----------------# 

# grid search for cost and gamma
cost.rf =10^(-3:3)
gamma.rf = gamma= 10^(-3:3)
# tune svm with radial kernel
svmrbf.tune <- tune.svm(digit ~ ., data = trainset, kernel = "radial", cost = cost.rf , gamma= gamma.rf,  scale= FALSE )
# select performances
perf.rbf = svmrbf.tune$performances[1:3]
# will plot in matlab
write.csv(perf.rbf, 'perf.rbf.csv') 

# select best performance
best.cost.rf = svmrbf.tune$best.parameters$cost
best.gamma.rf = svmrbf.tune$best.parameters$gamma

# train svm with best performance 
svmrbf.model <- svm(digit ~ ., data = trainset, kernel = "radial", cost =best.cost.rf, gamma = best.gamma.rf)
svmrbf.pred <- predict(svmrbf.model, xtest)
table(pred =svmrbf.pred, true = ytest)
mean(svmrbf.pred!=ytest)



#error.rbf = svmrbf.tune$performances$error
#err.mat = matrix(0, length(gamma.rf), length(cost.rf))
#row.names(err.mat) <- gamma.rf
#colnames(err.mat) <- cost.rf
#for (j in 1:length(cost.rf))
#  for (i in 1:length(gamma.rf))
#    err.mat[i,j]  = error.rbf[i + (j-1)*length(gamma.rf)]



